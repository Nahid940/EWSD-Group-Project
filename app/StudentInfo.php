<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model
{
    //

    protected $fillable=['user_id','department_id','session_id','batch_id'];

    public function department()
    {
        return $this->hasOne('App\Department','id','department_id');
    }

    public function session()
    {
        return $this->hasOne('App\SessionDepartment','id','session_id');
    }

    public function batch()
    {
        return $this->hasOne('App\Batch','id','batch_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->hasMany('App\Post');
    }
}
