<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControlIdeaPost extends Model
{
    //
    protected $fillable=['session_id','closure_date','final_close'];

    public function session()
    {
        return $this->belongsTo('App\SessionDepartment');
    }
}
