<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name', 'head_name'];

    public function sessions() {
    	return $this->hasMany('App\SessionDepartment');
    }

    public function studentinfo()
    {
        return $this->belongsTo('App\StudentInfo');
    }
    public function idea()
    {
        return $this->hasMany('App\Post');
    }
}
