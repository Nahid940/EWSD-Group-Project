<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $fillable = [
        'tag_id','user_id', 'title','body', 'profile_privacy', 'status','session_id','department_id',
    ];


    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function tag() {
    	return $this->belongsTo('App\Tag');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

     public function likes()
    {
        return $this->hasMany('App\PostLike');
    }

     public function postViwers()
    {
        return $this->hasMany('App\PostViwer');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function document()
    {
        return $this->hasOne('App\Document');
    }

    public function student()
    {
        return $this->belongsTo('App\StudentInfo');
    }


}
