<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
	protected $fillable = ['batch_id', 'name', 'description', 'status'];

    public function batch(){
    	return $this->belongsTo('App\Batch');
    }
}
