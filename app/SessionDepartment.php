<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionDepartment extends Model
{
    protected $fillable = ['department_id', 'name', 'start_date', 'end_date', 'status','can_post','final_close'];

    public function department() {
    	return $this->belongsTo('App\Department');
    }

    public function batches() {
    	return $this->hasMany('App\Batch');
    }

    public function studentinfo()
    {
        return $this->belongsTo('App\StudentInfo');
    }

    public function contorlIdeaPost()
    {
        return $this->hasOne('App\ControlIdeaPost','session_id');
    }
}
