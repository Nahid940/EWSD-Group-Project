<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable = ['session_department_id', 'name', 'description', 'status'];

    public function department_session() {
    	return $this->belongsTo('App\SessionDepartment','session_department_id');
    }

    public function studentinfo()
    {
        return $this->belongsTo('App\StudentInfo');
    }

    public function sections(){
    	return $this->hasMany('App\Section');
    }
}
