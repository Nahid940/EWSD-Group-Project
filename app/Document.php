<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{

    protected $fillable=['post_id','session_id','doc'];

    public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
