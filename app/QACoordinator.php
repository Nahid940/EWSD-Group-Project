<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QACoordinator extends Model
{
    protected  $fillable=['user_id','department_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function department()
    {
        return $this->hasOne('App\Department','id','department_id');
    }
}
