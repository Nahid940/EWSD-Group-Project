<?php

namespace App\Http\Controllers;

use App\ControlIdeaPost;
use App\QACoordinator;
use App\SessionDepartment;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index(Request $request)
    {
        $sessions = SessionDepartment::with(['department'])->get();
           if($request->ajax()) {
               
               return response()->json($sessions);
           }
           return view('admin.session-departments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $departments = Department::get(['id','name']);
        if($request->ajax()){
            return response()->json($departments);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnData = [];

        $sessionDepartment = new SessionDepartment;
        $newSessionDepartment = $sessionDepartment->create($request->all());

        if($newSessionDepartment){
            $returnData['alertType'] = 'alert-success';
            $returnData['alertMessage'] = 'Tag Save Successfully';
        }
        
        
        return json_encode($returnData);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SessionDepartment  $sessionDepartment
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $sessionDepartment = SessionDepartment::with('department')->where('id','=',$request->id)->first();
        if($request->ajax()) {
            //return json_encode($sessionDepartment->with(['department'])->get());
            return response()->json($sessionDepartment);
        }
        return "Not Ajax Call";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SessionDepartment  $sessionDepartment
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $sessionDepartment = SessionDepartment::with('department')->where('id','=',$request->id)->first();
        $departments = Department::get(['id','name']);
        if($request->ajax()) {
            return json_encode(array($sessionDepartment,$departments));
        }
        
       
        return "Not Ajax Call";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SessionDepartment  $sessionDepartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SessionDepartment $sessionDepartment)
    {
         if($request->ajax()){
            
            $sessionDepartment->update($request->all());

            return response()->json($request->all());
        }

        return "Not update Ajax Call";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SessionDepartment  $sessionDepartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, SessionDepartment $sessionDepartment)
    {
        $sessionDepartment->delete();

        if($request->ajax()){
            return "Deleted";
        }
    }


    public function getDeptWiseSession(Request $request)
    {
        $data=SessionDepartment::with('department')->where('department_id','=',$request->id)->get();
        return json_encode($data);
    }

    public function getsessiondata(Request $request)
    {
        $data=ControlIdeaPost::select('session_id','closure_date','final_close')->where('session_id',$request->id)->first();
        return $data;
    }

    public function manageideapost(Request $request)
    {

        ControlIdeaPost::where('session_id', $request->id)
            ->update(['closure_date' => $request->closure_date,'final_close'=>$request->final_close]);
    }

    public function manageideapostPermission()
    {
        $dpdid=QACoordinator::with('department')->where('user_id',Auth::user()->id)->first();
        $sessions=SessionDepartment::with('contorlIdeaPost')->where('department_id',$dpdid->department->id)->get();
        return view('admin.posts.manageideaposts',compact('sessions'));
    }

    public function newPErmitForIdea(Request $request)
    {
        ControlIdeaPost::create(['session_id'=>$request->id,'closure_date'=>$request->closure_date,'final_close'=>$request->final_close]);
    }

    public function existingDate(Request $request)
    {
        $d=ControlIdeaPost::select('closure_date','session_id','final_close')->where('session_id',$request->id)->first();
        return $d;
    }

    public function disableIdeaControl(Request $request)
    {
        ControlIdeaPost::where('session_id', $request->id)
        ->update(['closure_date' => $request->closure_date,'final_close'=>$request->final_close]);
    }
}
