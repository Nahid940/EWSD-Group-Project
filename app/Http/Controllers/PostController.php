<?php

namespace App\Http\Controllers;

use App\Department;
use App\Post;
use App\PostLike;
use App\PostViwer;
use App\QACoordinator;
use App\StudentInfo;
use App\User;
use App\Tag;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index()
    {

        $dpd_id=QACoordinator::select('department_id')->where('user_id',Auth::user()->id)->first();


       $likes= PostLike::with('post')->groupBy('post_id')
            ->selectRaw('post_id,user_id,sum(likes) as total_like')->where('likes','1')
            ->orderBy('total_like','desc')
            ->get();

        $views= PostViwer::with('post')->groupBy('post_id')
            ->selectRaw('post_id,user_id,count(user_id) as total_view')
            ->orderBy('total_view','desc')
            ->get();


        if(Auth::user()->hasRole('qacoordinator'))
        {
            $posts = Post::with(['user','tag','comment'])->where('department_id',$dpd_id->department_id)->orderBy('created_at','desc')->paginate(5);
            return view('admin.posts.index',compact(['posts','likes','views']));
        }else{
            $posts = Post::with(['user','tag','comment'])->orderBy('created_at','desc')->paginate(5);
            $d=StudentInfo::with('department','user')->get();
            return view('admin.posts.index',compact(['posts','likes','views']));
        }
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::get(['id','name']);
        return view('admin.posts.create',compact(['tags']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnData = [];
        $post = new Post;

        $post->user_id = $request->input('user_id');
        $post->tag_id = $request->input('tag_id');
        
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        //$post->profile_privacy = $request->input('profile_privacy');
        $post->status = $request->input('status');
//        $newPost = $post->create(['user_id'=>Auth::user()->id,'tag_id'=>$request->tag_id,'title'=>$request->title,'body'=>$request->body,'profile_privacy'=>$request->profile_privacy,'status'=>$request->status,'department_id'=>Auth::user()->de]);
        
         $post->save();

//        if($newPost){
//            $returnData['alertType'] = 'alert-success';
//            $returnData['alertMessage'] = 'Post Save Successfully';
//        }
         StudentInfo::select('department_id')->where('user_id',Auth::user()->id)->first();
        
        return redirect()->back();
        // return json_encode($newPost);
//        return json_encode("Heelo");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        //$post = Post::with(['user','tag','comment'])->where('department_id','=',$request->Auth::user()->id)->get(); 
        $post = Post::with(['user','tag','comment'])->where('department_id','=',Auth::user()->id)->get();

        dd($post);

            // $postComment = Comment::where('post_id',$post->id)->get();   

        //  if($request->ajax()) {
        //     return response()->json([$post,$postComment]);
        // }

        //return view('admin.posts.index',comment('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::with('tag')->where('id','=',$id)->first();
        $tags = Tag::get(['id','name']);
       
        return view('admin.posts.edit', compact(['post','tags']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
                   
        $post->update($request->all());

        return redirect('admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Post $post)
    {
        $comment = Comment::where('post_id',$post->id)->delete();

        $post->delete();

        return redirect()->back();
    }
}
