<?php

namespace App\Http\Controllers;

use App\ControlIdeaPost;
use App\Document;
use App\QACoordinator;
use App\StudentInfo;
use App\Tag;
use App\User;
use App\Post;
use App\Comment;
use App\PostLike;
use App\PostViwer;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $d=StudentInfo::with('session')->where('user_id',Auth::user()->id)->first();

        $tags = Tag::all();
        $postIdeaAndComment=[];


        $dpd_id=StudentInfo::select('department_id','session_id')->where('user_id',Auth::user()->id)->first();
        $posts = Post::with('comment','user')->where('department_id',$dpd_id->department_id)->where('status',1)->orderBy('created_at','desc')->paginate(5);

        $IdeaPostControl=ControlIdeaPost::select('closure_date','final_close')->where('session_id',$dpd_id->session_id)->first();

        if($IdeaPostControl!=null)
        {
            if($IdeaPostControl->closure_date < date('Y-m-d')&& $IdeaPostControl->final_close < date('Y-m-d'))
            {
                $postIdeaAndComment[]=['idea'=>'no','comment'=>'no'];

            }elseif ($IdeaPostControl->closure_date >=date('Y-m-d') && $IdeaPostControl->final_close >= date('Y-m-d'))
            {
                $postIdeaAndComment[]=['idea'=>'yes','comment'=>'yes'];

            }elseif($IdeaPostControl->closure_date < date('Y-m-d') && $IdeaPostControl->final_close >= date('Y-m-d')){
                $postIdeaAndComment[]=['idea'=>'no','comment'=>'yes'];
            }
        }
//        dd($postIdeaAndComment[0]);
        if($d->session!='')

        {
            return view('home',compact(['d','posts','tags','postIdeaAndComment','allLikes']));
        }
        else{
            $d=[];
            return view('home',compact(['d','posts','postIdeaAndComment','allLikes']));
        }

    }

    public function createPost()
    {
       $tags = Tag::all();
       return view('post', compact(['tags']));
    }

    public function postIdea (Request $request){
        $post = new Post;
        $dpd_id=StudentInfo::select('department_id','session_id')->where('user_id',Auth::user()->id)->first();

        if($request->hasFile('doc')) {
            $file = $request->doc;
            $extention = $file->getClientOriginalExtension();
            $filename = rand(111111, 999999) . "." . $extention;
            $file->move('uploads/files/', $filename);
            $doc = $filename;
            $newPost = $post->create(['user_id' => Auth::user()->id, 'tag_id' => $request->tag_id, 'title' => $request->title, 'body' => $request->body, 'profile_privacy' => $request->profile_privacy, 'status' => 0,'session_id'=> $dpd_id->session_id, 'department_id' => $dpd_id->department_id]);
            Document::create(['post_id' => $newPost->id,'session_id'=>$dpd_id->session_id, 'doc' => $doc]);
        }
        else{
            $newPost = $post->create(['user_id' => Auth::user()->id, 'tag_id' => $request->tag_id, 'title' => $request->title, 'body' => $request->body, 'profile_privacy' => $request->profile_privacy, 'status' => 0,'session_id'=> $dpd_id->session_id, 'department_id' => $dpd_id->department_id]);
        }


        $qac=QACoordinator::with('department')->where('department_id',$dpd_id->department_id)->first();

        $data = array('name'=>Auth::user()->name);
        Mail::send('admin_mail',$data, function($message) use ($qac) {

            $message->to($qac->user->email, 'E-idea')->subject
            ('Posted a new idea.');
            $message->from('idea@info','eIdea');
        });

        return redirect()->back()->with('alertMessage', 'Post submitted Successfully ! Please wait until it is approved by admin.');
    }

    public function postDetails (Request $request, $id){
        $post = Post::findOrFail($id);
        return view('post-details',compact(['post']));
    }

    public function postComment (Request $request) {
        $postComment = new Comment;
        $postComment->post_id = $request->input('post_id');
        $postComment->user_id = $request->input('user_id');
        $postComment->comment = $request->input('comment');
        if($request->anonymous=='')
        {
            $postComment->anonymous=1;
        }else{
            $postComment->anonymous = $request->input('anonymous');
        }

        $user=Post::with('user')->where('id',$request->post_id)->first();

          if(Auth::user()->hasRole('student'))
            {
                $data = array('name'=>Auth::user()->name,'comment'=>$request->comment);

                if($user->user->id!=Auth::user()->id)
				{
					Mail::send('comment_mail',$data, function($message) use ($user) {

                    $message->to($user->user->email, 'E-idea')->subject
                    ('Commented on your post');
                    $message->from('idea@info','eIdea');
					});
				}
            }

        $postComment->save();
        return redirect()->back()->with('commented','Comment posted !!');
    }

    public function postLike(Request $request) {
        $existData=PostLike::select('id','user_id','post_id','likes','dislike')
            ->where('user_id',$request->user_id)
            ->where('post_id',$request->post_id)
            ->first();


        if($existData!=null)
        {
            if($request->Buttonaction=='like')
            {
                if($existData->likes==$request->like)
                {
                    return "done";
                }else{
                    DB::table('post_likes')
                        ->where('id', $existData->id)
                        ->update(['likes' => '1','dislike' => '0']);
                    return ["liked",'id'=>$existData->post_id];
                }

            }elseif($request->Buttonaction=='dislike')
            {

                if($existData->dislike==$request->dislike)
                {
                    return "done";
                }else{
                    DB::table('post_likes')
                        ->where('id', $existData->id)
                        ->update(['likes' => '0','dislike' => '-1']);
                    return ["disliked",'id'=>$existData->post_id];
                }
            }

        }else
        {
            $d=PostLike::create(['user_id'=>$request->user_id,'post_id'=>$request->post_id,'likes'=>$request->like,'dislike'=>$request->dislike]);
            return ["newreocrdadded"=>'ok','id'=>$request->post_id];
        }

    }


    public function postView (Request $request){
        $postViwer = new PostViwer;
        $newPostViwer= $postViwer->create($request->all());

        return "saved";
    }

    public function profile()
    {
        $data=User::with('student')->where('id',Auth::user()->id)->first();
        return view('profile',compact('data'));
    }
}
