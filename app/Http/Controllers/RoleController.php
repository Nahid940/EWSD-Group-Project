<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index()
    {
        $roles = Role::all();
        return view('admin.roles.index', compact(['roles']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $role = new Role;

        $this->validate($request,[
              'designation' => 'required|unique:roles|max:100',
              'salary' => 'required',
        ],[
              'designation.required' => ' The designation field is required.',
              'designation.max' => ' The designation may not be greater than 100 characters.',
              'designation.unique' => ' It seems designation already exist',
              'salary.required' => ' The salary field is required.',
        ]);

        $role->designation = $request->input('designation');
        $role->role_name = strtolower(str_replace(' ', '', $request->input('designation')));
        $role->salary = $request->input('salary');

       Session::flash('success','Role Successfully Added');

        $role->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return view('admin.roles.show', compact(['role']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('admin.roles.edit', compact(['role']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $this->validate($request,[
            'salary' => 'required',
            'designation' => 'required|max:100|unique:roles,designation,'.$role->id
              
        ],[
              'designation.required' => ' The designation field is required.',
              'designation.max' => ' The designation may not be greater than 100 characters.',
              'designation.unique' => ' It seems designation already exist',
              'salary.required' => ' The salary field is required.',
        ]);

         $role->designation = $request->input('designation');
         $role->salary = $request->input('salary');



        Session::flash('update','Role Successfully Update');

         $role->save();
         return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        Session::flash('delete','Role Successfully Delete');
        return redirect('admin/roles');
    }
}
