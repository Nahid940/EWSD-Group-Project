<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
            $tags = Tag::all();
            return json_encode($tags);
        }

        $contributor=DB::table('posts')->select('tags.name as tagname',DB::raw('COUNT( tag_id) as totaltag '))->groupBy('tag_id')
            ->leftJoin('tags','tags.id','=','posts.tag_id')
            ->get();

        return view('admin.tags.index',compact('contributor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnData = [];

        $tag = new Tag;
        $newTag = $tag->create($request->all());

        if($newTag){
            $returnData['alertType'] = 'alert-success';
            $returnData['alertMessage'] = 'Tag Save Successfully';
        }


        
        
        return json_encode($returnData);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Tag $tag)
    {
        if($request->ajax()) {
            return json_encode($tag);
        }

        return "Not Ajax Call";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Tag $tag)
    {
        if($request->ajax()){
            return json_encode($tag);
        }
        return "Not Ajax Call";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        if($request->ajax()){
            $tag->update($request->all());
            return "Update";
        }

        return "Not update Ajax Call";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tag $tag)
    {
        $tag->delete();

        if($request->ajax()){
            return "Deleted";
        }
    }
}
