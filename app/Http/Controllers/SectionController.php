<?php

namespace App\Http\Controllers;

use App\Section;
use App\Batch;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $sections = Section::with(['batch'])->get();
            //return "Hello";
            return response()->json($sections);
        }
        return view('admin.sections.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $batches = Batch::get(['id','name']);
        if($request->ajax()){
            return response()->json($batches);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnData = [];

        $section = new Section;
        $newSection = $section->create($request->all());

        if($newSection){
            $returnData['alertType'] = 'alert-success';
            $returnData['alertMessage'] = 'Tag Save Successfully';
        }
        
        
        return json_encode($returnData);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $section = Section::with('batch')->where('id','=',$request->id)->first();
        if($request->ajax()) {
            //return json_encode($sessionDepartment->with(['department'])->get());
            return response()->json($section);
        }
        return "Not Ajax Call";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $section = Section::with('batch')->where('id','=',$request->id)->first();
        $batch = Batch::get(['id','name']);
        if($request->ajax()) {
            return json_encode(array($section,$batch));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        if($request->ajax()){
            
            $section->update($request->all());

            return response()->json($request->all());
        }

        return "Not update Ajax Call";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Section $section)
    {
        $section->delete();

        if($request->ajax()){
            return "Deleted";
        }
    }
}
