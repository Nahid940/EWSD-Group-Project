<?php

namespace App\Http\Controllers\Idea;

use App\Comment;
use App\ControlIdeaPost;
use App\Document;
use App\Post;
use App\QACoordinator;
use App\SessionDepartment;
use App\StudentInfo;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use ZipArchive;

class IdeaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }


    public function newidea(Request $request)
    {
        $deptID=QACoordinator::with('department')->where('user_id',Auth::user()->id)->first();
        if($request->ajax()) {
            $posts = Post::with('user','document')->where('department_id',$deptID->department->id)->where('status',0)->get();
            return $posts;
        }
    }

    public function enableidea()
    {
        $dptid=QACoordinator::select('department_id')->where('user_id',Auth::user()->id)->first();

        $data=SessionDepartment::with('department')->where('department_id',$dptid->department_id)->paginate(5);

        $data=SessionDepartment::with('contorlIdeaPost')->where('department_id',$dptid->department_id)->get();
//        dd($data);
        return view('admin.idea.manageidea',compact('data'));
    }

    public function approve(Request $request)
    {
        Post::where('id', $request->id)
            ->update(['status' => 1]);
    }
    public function deletePost(Request $request)
    {
        DB::table('posts')->where('id',$request->id)->delete();
    }


    public function approvenewidea()
    {
        $docs=Document::select('doc')->get();
        return view('admin.posts.approvenewidea',compact('docs'));
    }

    public function getuserinfo(Request $request)
    {
        $d=StudentInfo::with('user','department','session','batch')->where('user_id',$request->id)->first();
        return $d;
    }

    public function allnewPosts()
    {
        $deptID=QACoordinator::with('department')->where('user_id',Auth::user()->id)->first();
        $d=Post::with('user')->where('status',0)->where('department_id',$deptID->department->id)->get();
        return $d;
    }

    public function no_idea_comments()
    {
        $nocommentIdeas=array();
        $d=Post::with('comment','user')->get();
        for($i=0;$i<sizeof($d);$i++)
        {
            if((sizeof($d[$i]->comment)==0))
            {
                $nocommentIdeas[]=["body"=>$d[$i]->body,'id'=>$d[$i]->id,'user_name'=>$d[$i]->user->name];
            }
        }
        return view('admin.posts.ideawithnocomments',compact('nocommentIdeas'));
    }

    public function individualIdeaWithNoComment($id)
    {
        $d=Post::with('user')->where('id',$id)->first();
        $info=StudentInfo::with('department','session','batch')->where('user_id',$d->user->id)->first();
        return view('admin.posts.individualIdeaWithNoComment',compact('d','info'));
    }

    public function annomynousIdeaAndComment()
    {
        $annonidea=Post::with('user')->where('profile_privacy',0)->paginate(5);
        $annonComment=Comment::with('post','user')->where('anonymous',0)->paginate(5);
        return view('admin.posts.annonymousIdeasAndComment',compact('annonidea','annonComment'));
    }

    public function categorywiseidea()
    {
        $tags=Tag::select('id','name')->get();
        return view('admin.posts.categroyiwseidea',compact('tags'));
    }

    public function getallideaByTag(Request $request)
    {
        $idea=Post::with('tag','user')->where('tag_id',$request->id)->orderBy('created_at','desc')->get();
        return $idea;
    }

    public function alldocs()
    {

        $session=ControlIdeaPost::with('session')->where('final_close', '<', date('Y-m-d'))->get();

        $File=Document::with('post')->get();

        $ssn_data=Document::select('session_id')->distinct()->get();

        if(sizeof($ssn_data)>=1)
        {
            $empty='not';
            foreach ($ssn_data as $sid)
            {
                $d=SessionDepartment::select('id','name')->where('id',$sid->session_id)->first();
                $session_data[]=['id'=>$d->id,'name'=>$d->name];
            }

            return view('admin.posts.alldocs',compact('session_data','File','empty'));
        }

        $empty='';
        return view('admin.posts.alldocs',compact('empty'));


    }

    public function getallFile(Request $request)
    {
        $files=Document::select('doc')->where('session_id',$request->session_id)->get();

//        dd($files);

        $public_dir=public_path().'/uploads/files';
        $zipFileName = time().'.zip';

        $zip = new ZipArchive;

        for($i=0;$i<sizeof($files);$i++)
        {
            if($files[$i]->doc!=null)
            {
                if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
                        $zip->addFile('uploads/files/' . $files[$i]->doc);
                    $zip->close();
                }
            }
        }

        if (file_exists($public_dir . '/' . $zipFileName)) {
            return response()->download($public_dir . '/' . $zipFileName, $zipFileName);
        }


    }

    public function myidea()
    {
        $d=Post::with('comment','tag')->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->paginate(5);
        return view('myidea',compact('d'));
    }

    public function deleteIdea(Request $request)
    {
        $idea = Post::findOrFail($request->idea_id);
        $idea->delete();
        return redirect()->back()->with('delete','Idea deleted !!');

    }

    public function editmyidea($id){
        $post = Post::findOrFail($id);
        //$postDocument = Document::where('post_id',$post->id)->get();
        $tags = Tag::all();

        return view('post',compact(['post','tags']));
    }

    public function updateIdea(Request $request,Post $post){

        //dd($id);

         //$post = Post::findOrFail($id);
        //dd($post);
        $post->tag_id = $request->input('tag_id');
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->profile_privacy = $request->input('profile_privacy');
        $post->created_at =$request->input('post_date');

        $files=Document::select('doc')->where('post_id',$post->id)->get();

        if($request->hasFile('doc')){
        $file = $request->file('doc');
        $fileExt = $file->getClientOriginalExtension();

        $newFileName = time() . '.' . $fileExt;

        $destinationPath = '/uploads/files';

       if($post->document['doc']) {
           unlink(public_path('/uploads/files' . $destinationPath . $post->document->doc));
        }
        
        $file->storeAs($destinationPath, $newFileName, '/uploads/files');

        $files->doc = $newFileName;
        $files->save();
            
        }
        
        $post->save();

        return redirect('myidea');
    }
}
