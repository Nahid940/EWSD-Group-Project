<?php

namespace App\Http\Controllers;

use App\QACoordinator;
use App\StudentInfo;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index(Request $request)
    {
        $users = User::with(['role'])->get();


        if($request->ajax()) {
            return response()->json($users);
        }
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $roles = Role::get(['id','designation','role_name']);
        if($request->ajax()){
            return response()->json($roles);
        }
        //return view('admin.users.index', compact(['roles']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $returnData = [];

        $user = new User;

        if($request->hasFile('image')){
            $file = $request->file('image');
            $fileExt = $file->getClientOriginalExtension();

            $newFileName =  time() . '.' . $fileExt;

            $destinationPath = 'users/';

           if($user->image) {
               unlink(public_path('uploads/' . $destinationPath . $user->image));
            }
            $file->storeAs($destinationPath, $newFileName, 'uploads');

            $user->image = $newFileName;

        }

        $user->role_id = $request->input('role_id');
        $user->name = $request->input('name');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->image = $newFileName;
        $user->status = $request->input('status');

        $newUser = $user->save();

        if($request->role_name_input=='student')
        {
            StudentInfo::create(['user_id'=>$user->id,'department_id'=>$request->department_id,'session_id'=>$request->session_id,'batch_id'=>$request->batch_id]);
        }
        else if($request->role_name_input=='qacoordinator')
        {
            QACoordinator::create(['user_id'=>$user->id,'department_id'=>$request->department_id]);
        }


        if($newUser){
            $returnData['alertType'] = 'alert-success';
            $returnData['alertMessage'] = 'User Save Successfully';
        }

        return json_encode($returnData);
        //return "Helllo"; 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $user=User::with('role')->where('id','=',$request->id)->first();

        if($user->role->role_name=='student')
        {
            $user=StudentInfo::with('user','department','session')->where('user_id',$request->id)->first();
            return response()->json([$user,'student']);

        }elseif($user->role->role_name=='qacoordinator')
        {
            $user=QACoordinator::with('user','department')->where('user_id',$request->id)->first();
            return response()->json([$user,'qacoordinator']);
        }else{
            return response()->json($user);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
        if($request->ajax()) {
            return ($user::with('role')->where('id','=',$request->id)->first());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $oldimage=User::select('image')->where('id','=',$request->id)->first();

        if($request->hasFile('image'))
        {
            $file=$request->image;
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $file->move('uploads/users/',$filename);
            $photo=$filename;
            $filename=($photo);
            unlink('uploads/users/'.$oldimage->image);
//            Storage::delete('uploads/users/'.$oldimage->image);
//            Storage::delete('users/'.$oldimage->image);

            $data=['name'=>$request->name,
                'email'=>$request->email,
                'address'=>$request->address,
                'phone'=>$request->phone,
                'image'=>$photo,
                ];
            \Illuminate\Support\Facades\DB::table('users')
                ->where('id', $request->id)
                ->update($data);
            return "updated";
        }else{
            $data=['name'=>$request->name,
                'email'=>$request->email,
                'address'=>$request->address,
                'phone'=>$request->phone,
                'image'=>$oldimage->image];
            \Illuminate\Support\Facades\DB::table('users')
                ->where('id', $request->id)
                ->update($data);

            return "updated";
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();

        if($request->ajax()){
            return "Deleted";
        }
    }
}
