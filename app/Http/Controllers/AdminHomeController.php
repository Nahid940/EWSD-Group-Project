<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Comment;
use App\Department;
use App\Post;
use App\QACoordinator;
use App\Role;
use App\SessionDepartment;
use App\StudentInfo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminHomeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }
    public function index(Request $request)
    {

        if(Auth::user()->hasRole('admin'))
        {
            $role_id=Role::where('role_name','student')->first();
            $dept=Department::count();
            $batch=Batch::count();
            $student=StudentInfo::count();
            $staff=User::where('role_id','!=',$role_id->id)->count();

            return view('admin.index',compact('dept','batch','student','staff'));
        }
        elseif (Auth::user()->hasRole('qacoordinator'))
        {
            $myDept=QACoordinator::with('user','department')->where('user_id',Auth::user()->id)->first();

             if($myDept->department!=null)
             {
                 $totalstudent=StudentInfo::where('department_id',$myDept->department->id)->count();
                 $session=SessionDepartment::where('department_id',$myDept->department->id)->count();

                 if($totalstudent>=1 && $session>=1)
                 {
                     return view('admin.index',compact('myDept','totalstudent','session'));
                 }else{
                     $totalstudent='';
                     $session='';
                     return view('admin.index',compact('myDept','totalstudent','session'));
                 }
             }else{
                 $totalstudent='';
                 $session='';
                 return view('admin.index',compact('myDept','totalstudent','session'));
             }

        }
        elseif (Auth::user()->hasRole('qualityassurancemanager'))
        {

            $x=Post::select('department_id','name',DB::raw('COUNT(department_id) as total '))
                ->leftJoin('departments','departments.id','=','posts.department_id')
                ->groupBy('department_id')->get();
            $totalIdea=Post::count();

            $contributor=DB::table('posts')->select('departments.name as dname',DB::raw('COUNT(DISTINCT user_id) as contributor '))->groupBy('department_id')
                ->leftJoin('departments','departments.id','=','posts.department_id')
                ->get();

            $annonidea=Post::with('user')->where('profile_privacy',0)->count();
            $annonComment=Comment::with('post','user')->where('anonymous',0)->count();

            return view('admin.index',compact('contributor','x','annonidea','annonComment','totalIdea'));
        }

    }

    public function chart()
    {
        $x=DB::table('student_infos')->select('departments.name as dname',DB::raw('COUNT(user_id) as totalstudent'))->groupBy('department_id')
            ->leftJoin('departments','departments.id','=','student_infos.department_id')->get();
        return $x;
    }

    public function total_post_by_each_dept(){
        $x=DB::table('posts')->select('departments.name as dname',DB::raw('COUNT(department_id) as total_idea_of_each_dept'))->groupBy('department_id')
            ->leftJoin('departments','departments.id','=','posts.department_id')->get();
        return $x;
    }
}
