<?php

namespace App\Http\Controllers\Student;

use App\StudentInfo;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function allstudents()
    {
        $data=StudentInfo::with('user','department','batch','session')->get();
        return view('admin.student.allstudents',compact('data'));
    }

    public function deptwiseallstudents()
    {
        $deptid=User::with('qacoordinator')->where('id',Auth::user()->id)->first();
        $data=StudentInfo::with('user','department','batch','session')->where('department_id',$deptid->qacoordinator->department_id)->get();
        return view('admin.student.allstudents',compact('data'));
    }

}
