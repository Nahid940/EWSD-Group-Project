<?php

namespace App\Http\Controllers;

use App\Batch;
use App\SessionDepartment;
use Illuminate\Http\Request;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function index(Request $request)
    {
        
        if($request->ajax()) {
            $batches = Batch::with(['department_session'])->get();
            //return "Hello";
            return response()->json($batches);
        }
        return view('admin.batches.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $sessionDepartment = SessionDepartment::get(['id','name']);
        if($request->ajax()){
            return response()->json($sessionDepartment);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnData = [];

        $batch = new Batch;
        $newBatch = $batch->create($request->all());

        if($newBatch){
            $returnData['alertType'] = 'alert-success';
            $returnData['alertMessage'] = 'Tag Save Successfully';
        }
        
        
        return json_encode($returnData);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $batch = Batch::with('department_session')->where('id','=',$request->id)->first();
        if($request->ajax()) {
            //return json_encode($sessionDepartment->with(['department'])->get());
            return response()->json($batch);
        }
        return "Not Ajax Call";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $batch = Batch::with('department_session')->where('id','=',$request->id)->first();
        $sessionDepartment = SessionDepartment::get(['id','name']);
        if($request->ajax()) {
            return json_encode(array($batch,$sessionDepartment));
        }
        
       
        return "Not Ajax Call";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Batch $batch)
    {
         if($request->ajax()){
            
            $batch->update($request->all());

            return response()->json($request->all());
        }

        return "Not update Ajax Call";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Batch  $batch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Batch $batch)
    {
        $batch->delete();

        if($request->ajax()){
            return "Deleted";
        }
    }

    public function getSessionWiseBatch(Request $request)
    {
        $data=Batch::with('department_session')->where('session_department_id','=',$request->id)->get();
        return json_encode($data);
    }
}
