<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
})->middleware('guest');

Route::group(['middleware'=>'auth','role'=>['admin','qacoordinator','qualityassurancemanager']], function() {
    Route::resource('admin/', 'AdminHomeController');
    Route::resource('admin/posts', 'PostController');
});

Route::group(['middleware'=>'auth','role'=>['qualityassurancemanager']], function() {
    Route::resource('admin/tags', 'TagController');
});

Route::group(['middleware'=>'auth','role'=>['admin']], function(){
    Route::resource('admin/roles', 'RoleController');
    Route::resource('admin/departments', 'DepartmentController');
    Route::resource('admin/users', 'UserController');
    Route::resource('admin/session-departments', 'SessionDepartmentController');
    Route::resource('admin/batches', 'BatchController');
    Route::resource('admin/sections', 'SectionController');
});


Route::get('/getDeptWiseSession',['uses'=>'SessionDepartmentController@getDeptWiseSession','role'=>'admin','as'=>'getDeptWiseSession']);
Route::post('/getsessiondata',['uses'=>'SessionDepartmentController@getsessiondata','role'=>'qacoordinator','as'=>'getsessiondata']);
Route::post('/manageideapost',['uses'=>'SessionDepartmentController@manageideapost','role'=>'qacoordinator','as'=>'manageideapost']);
Route::post('/newPErmitForIdea',['uses'=>'SessionDepartmentController@newPErmitForIdea','role'=>'qacoordinator','as'=>'newPErmitForIdea']);
Route::post('/existingDate',['uses'=>'SessionDepartmentController@existingDate','role'=>'qacoordinator','as'=>'existingDate']);
Route::post('/disableIdeaControl',['uses'=>'SessionDepartmentController@disableIdeaControl','role'=>'qacoordinator','as'=>'disableIdeaControl']);
Route::get('/manageideapostPermission',['uses'=>'SessionDepartmentController@manageideapostPermission','role'=>'qacoordinator','as'=>'manageideapostPermission']);
Route::get('/getSessionWiseBatch',['uses'=>'BatchController@getSessionWiseBatch','role'=>'admin','as'=>'getSessionWiseBatch']);
Route::get('/allstudents',['uses'=>'Student\StudentController@allstudents','role'=>'admin','as'=>'allstudents']);
Route::get('/deptwiseallstudents',['uses'=>'Student\StudentController@deptwiseallstudents','role'=>'qacoordinator','as'=>'deptwiseallstudents']);
Route::get('/enable-post-idea',['uses'=>'Idea\IdeaController@enableidea','role'=>'qacoordinator','as'=>'enable-post-idea']);
Route::get('/alldocs',['uses'=>'Idea\IdeaController@alldocs','role'=>'qualityassurancemanager','as'=>'alldocs']);
Route::get('/approvenewidea',['uses'=>'Idea\IdeaController@approvenewidea','role'=>'qacoordinator','as'=>'approvenewidea']);
Route::get('/newidea',['uses'=>'Idea\IdeaController@newidea','role'=>'qacoordinator','as'=>'newidea']);
Route::post('/approve',['uses'=>'Idea\IdeaController@approve','role'=>'qacoordinator','as'=>'approve']);
Route::post('/deletePost',['uses'=>'Idea\IdeaController@deletePost','role'=>'qacoordinator','as'=>'deletePost']);
Route::post('/getuserinfo',['uses'=>'Idea\IdeaController@getuserinfo','role'=>'qacoordinator','as'=>'getuserinfo']);
Route::get('/allnewPosts',['uses'=>'Idea\IdeaController@allnewPosts','role'=>'qacoordinator','as'=>'allnewPosts']);
Route::get('/no_idea_comments',['uses'=>'Idea\IdeaController@no_idea_comments','role'=>['qacoordinator','qualityassurancemanager'],'as'=>'no_idea_comments']);
Route::get('/individualIdeaWithNoComment/{id}',['uses'=>'Idea\IdeaController@individualIdeaWithNoComment','role'=>['qacoordinator','qualityassurancemanager'],'as'=>'individualIdeaWithNoComment']);
Route::get('/annomynousIdeaAndComment',['uses'=>'Idea\IdeaController@annomynousIdeaAndComment','role'=>['admin','qacoordinator','qualityassurancemanager'],'as'=>'annomynousIdeaAndComment']);
Route::get('/categorywiseidea',['uses'=>'Idea\IdeaController@categorywiseidea','role'=>['admin','qacoordinator','qualityassurancemanager'],'as'=>'categorywiseidea']);
Route::post('/getallideaByTag',['uses'=>'Idea\IdeaController@getallideaByTag','role'=>['admin','qacoordinator','qualityassurancemanager'],'as'=>'getallideaByTag']);
Route::get('/chart',['uses'=>'AdminHomeController@chart','as'=>'chart','role'=>'admin']);
Route::get('/total_post_by_each_dept',['uses'=>'AdminHomeController@total_post_by_each_dept','as'=>'total_post_by_each_dept','role'=>['admin','qualityassurancemanager']]);



Auth::routes();
Route::get('/profile',['uses'=>'HomeController@profile','as'=>'profile','role'=>['student']]);

Route::get('/', ['uses'=>'HomeController@index','as'=>'home','role'=>['student']]);

Route::get('/post', ['uses'=>'HomeController@createPost','as'=>'createPost','role'=>['student']]);

Route::post('/post', ['uses'=>'HomeController@postIdea','as'=>'post','role'=>['student']]);

Route::get('/post/show/{id}', ['uses'=>'HomeController@postDetails','as'=>'post-details','role'=>['student']]);


Route::post('/comment', ['uses'=>'HomeController@postComment','as'=>'comment','role'=>['student','qacoordinator','qualityassurancemanager','admin']]);

Route::post('/postLike', ['uses'=>'HomeController@postLike','as'=>'postLike','role'=>['student','qacoordinator','qualityassurancemanager','admin']]);

Route::post('/postView', ['uses'=>'HomeController@postView','as'=>'postView','role'=>['student','qacoordinator','qualityassurancemanager','admin']]);


Route::post('/getallFile',['uses'=>'Idea\IdeaController@getallFile','role'=>'qualityassurancemanager','as'=>'getallFile']);
Route::get('/myidea',['uses'=>'Idea\IdeaController@myidea','role'=>'student','as'=>'myidea']);
Route::post('/myidea',['uses'=>'Idea\IdeaController@deleteIdea','role'=>'student','as'=>'deleteIdea']);
Route::get('/myidea/{id}/edit',['uses'=>'Idea\IdeaController@editMyIdea','role'=>'student','as'=>'editmyidea']);
Route::post('/myidea/{post}',['uses'=>'Idea\IdeaController@updateIdea','role'=>'student','as'=>'updatemyidea']);

