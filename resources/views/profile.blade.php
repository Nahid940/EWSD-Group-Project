@extends('layouts.app')

@section('style')
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
    <style type="text/css">

        input.rounded {
            margin-right: 10px;
            border: 1px solid #ccc;
            -moz-border-radius: 20px;
            -webkit-border-radius: 20px;
            border-radius: 20px;
            -moz-box-shadow: 2px 2px 3px #666;
            -webkit-box-shadow: 2px 2px 3px #666;
            box-shadow: 2px 2px 3px #666;
            font-size: 18px;
            padding: 4px 7px;
            outline: 0;
            -webkit-appearance: none;
        }

        .glyphicon.glyphicon-comment{
            font-size: 25px;
        }
        a:link {
            text-decoration: none;
        }
        a:hover {
            text-decoration: none;
        }
        p{
            font-size: 14px;
            font-family: 'Slabo 27px', serif;
        }

        #idea{
            font-weight:bold;
        }
        .borderless td{
            color: #1c0000;
            border-top:1px solid #fff;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Your profile
                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">

                                <div class="modal-dialog modal-lg" role="document">

                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="table-responsive">
                                                <table class="table borderless">
                                                    <tr>
                                                        <td colspan="2"> <img src="{{asset('uploads/users/'.$data->image)}}" alt="" width="200px"></td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            Name: {{$data->name}}
                                                        </td>
                                                        <td>Email: {{$data->email}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Address: {{$data->address}}</td>
                                                        <td>Phone: {{$data->phone}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td>Department: {{$data->student->department->name}}</td>
                                                        <td>Session: {{$data->student->session->name}}</td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2">Batch: {{$data->student->batch->name}}</td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog --
                        </div>
                    </div>


            </div>
        </div>
    </div>


@endsection



