@extends('layouts.app')

@section('style')
<link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
<style type="text/css">

    input.rounded {
        margin-right: 10px;
        border: 1px solid #ccc;
        -moz-border-radius: 20px;
        -webkit-border-radius: 20px;
        border-radius: 20px;
        -moz-box-shadow: 2px 2px 3px #666;
        -webkit-box-shadow: 2px 2px 3px #666;
        box-shadow: 2px 2px 3px #666;
        font-size: 18px;
        padding: 4px 7px;
        outline: 0;
        -webkit-appearance: none;
    }

    .glyphicon.glyphicon-comment{
        font-size: 25px;
    }
    a:link {
        text-decoration: none;
    }
    a:hover {
        text-decoration: none;
    }
    p{
        font-size: 14px;
        font-family: 'Slabo 27px', serif;
    }

    #idea{
        font-weight:bold;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            @if(session('delete'))
            <div class="alert alert-info">
                {{session('delete')}}
            </div>
            @endif

            @if($d!=null)
            <div class="panel panel-primary">
                <div class="panel-heading">
                    All the idea you have posted !!
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body">
                    @foreach ($d as $post)
                    <div class="modal-dialog modal-lg" role="document">

                        <div class="modal-content">

                            <div class="modal-header">
                                <div class="pull-right">
                                    <button class="btn btn-default deletebutton" style="background-color: #f7091d;color:#fff" title="Remove post" data-toggle="modal" data-target="#myModal" data-id="{{$post->id}}"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
                                   
                                    <a style="background-color: #006622;color:#fff" type="button" href="{{ route('editmyidea', ['post' => $post->id]) }}" class="btn btn-default fa fa-pencil" title="Edit post"></a>
                                </div>
                                {{--<h2 class="modal-title"><a href="{{ route('post-details', ['id' => $post->id]) }}">{{ $post->title }}</a></h2>--}}
                                <p class="lead">

                                    {{$post->user->name}}

                                </p>
                                <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $post->created_at->diffForHumans() }}
                                </p>
                            </div>
                            <div class="modal-body">
                                <p id="idea"><i class="fa fa-lightbulb-o" aria-hidden="true" style="color: #fc5507;font-size: 30px"></i> {{ $post->body }}</p>
                                <a href="javascript:void(0)" data-toggle="collapse" data-target="#comments_{{ $post->id }}"><b>See all comments</b></a>
                            </div>
                            <div class="modal-footer">
                                <a type="button" style="cursor: pointer" class="glyphicon glyphicon-comment" title="Leave a Comments" data-toggle="collapse" data-target="#comments_{{ $post->id }}" aria-expanded="false" aria-controls="comments_{{ $post->id }}"></a>
                                <div class="collapse" id="comments_{{ $post->id }}">
                                    <div class="well well-sm pull-left">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form class="form-inline" method="post" action="{{ url('comment') }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id}}">
                                                    <div class="form-group">
                                                        <input size="60" name="comment" class="form-control rounded" type="text" placeholder=" Write a comment......">
                                                    </div>
                                                    <div style="margin-right: 10px;" class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="anonymous" value="0"> Hide profile
                                                            {{--<input type="hidden" name="anonymous" value="0">--}}
                                                        </label>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary ">Comment</button>
                                                </form><hr>
                                                <!-- Comment -->
                                                @foreach ($post->comment as $comment)
                                                @if($comment->user->role->role_name=='student')
                                                <div class="media">
                                                    <a class="pull-left" href="#">
                                                        @if ($comment->anonymous == 1)
                                                        <img class="media-object" src="{{asset('uploads/users/'.$post->user->image)}}" alt="" height="30px" width="30px">
                                                        @else
                                                        <img class="media-object" src="{{asset('image/anon.png')}}" alt="" height="30px" width="30px">
                                                        @endif
                                                    </a>
                                                    <div class="media-body">
                                                        <p style="text-align: left;">

                                                            @if ($comment->anonymous == 1)
                                                            Comment by {{$comment->user->name}}
                                                            @else
                                                            Comment by <i class="fa fa-user-secret" aria-hidden="true" style="color: #ff3838;"></i> Anonymous
                                                            @endif
                                                            <small>{{$comment->created_at->diffForHumans()}}</small><br>
                                                            <i class="fa fa-comments" aria-hidden="true" style="color: #56ccff"></i> {{ $comment->comment}}
                                                        </p>
                                                        @endif
                                                        {{--@endif--}}
                                                        {{--</p>--}}

                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <hr>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->

                    @endforeach

                    {{$d->links()}}
                </div>
            </div>
            @endif

        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-body">
                <strong><p>Edit your post </p></strong>
            </div>
            <div class="modal-footer">
                <form action="{{route('deleteIdea')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="idea_id" id="idea_id">
                    <button type="submit" class="btn btn-default" style="background-color: #ff2323;color: #fff;">Yes</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                </form>
            </div>

        </div>

    </div>
</div>

    @endsection

    @section('script')

    <script>
        $(document).on('click','.deletebutton',function () {
            $('#idea_id').val($(this).data('id'));

        });

        {{--$('#deleteButton').on('click', deleteSection);--}}

        {{--function deleteSection() {--}}
            {{--var deleteableSectionId =  $('#idea_id').val();--}}
            {{--var deleteUrl = '{{ route('sections.destroy', ['section' => ':sectionId']) }}';--}}
            {{--deleteUrl = deleteUrl.replace(':sectionId', deleteableSectionId);--}}

            {{--$.ajax({--}}
                {{--url : deleteUrl,--}}
                {{--type : 'POST',--}}
                {{--data : {--}}
                    {{--"_token": '{{ csrf_token() }}',--}}
                    {{--"_method": 'DELETE',--}}
                    {{--id:deleteableSectionId--}}
                {{--},--}}
                {{--success: function(result) {--}}
                    {{--loadSection();--}}
                    {{--$('#deleteConfirmationModel').modal('hide');--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}
    </script>
    @endsection


