<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>e-Idea</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

    @yield('css')



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('index')}}">e-Idea</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('qacoordinator'))
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bell"></i> <span id="totalnewidea"  class="badge" style="color: red;"></span> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown" id="newideas">
                    </ul>
                </li>
                @endif
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">

                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
						
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="{{url('admin/')}}"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                    </li>
                    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    <li>
                        <a href="{{ route('posts.index') }}"><i class="fa fa-lightbulb-o" style="color: #ff5100;"></i> View all ideas</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-sliders" aria-hidden="true"></i> Manage <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            {{--<li>--}}
                                {{--<a href="{{ route('roles.index') }}"><i class="far fa-life-ring"></i> Manage Role</a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{ route('departments.index') }}"><i class="fa fa-building" aria-hidden="true"></i> Manage Departments</a>
                            </li>
                            <li>
                                <a href="{{ route('session-departments.index') }}"><i class="fa fa-clock-o" aria-hidden="true"></i> Manage Sessions</a>
                            </li>
                            <li>
                                <a href="{{ route('batches.index') }}"><i class="fa fa-id-badge" aria-hidden="true"></i> Manage Batch</a>
                            </li>
                            {{--<li>--}}
                                {{--<a href="{{ route('sections.index') }}"><i class="fa fa-fw fa-desktop"></i> Manage Section</a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                        <li>
                            <a href="{{ route('users.index') }}"><i class="fa fa-users" aria-hidden="true"></i> Manage Users</a>
                        </li>
                    <li>
                        <a href="{{route('allstudents')}}"><i class="fa fa-graduation-cap" aria-hidden="true"></i>  All students</a>
                    </li>
                    @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('qacoordinator'))
                    <li>
                        <a href="{{ route('posts.index') }}"><i  class="fa fa-lightbulb-o" style="color: #ff5100;"></i> View all ideas</a>
                    </li>
                    <li>
                        <a href="{{route('approvenewidea')}}"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Approve new ideas</a>
                    </li>
                    <li>
                        <a href="{{route('manageideapostPermission')}}"><i class="fa fa-cog" aria-hidden="true"></i> Manage idea post</a>
                    </li>
                    <li>
                        <a href="{{route('categorywiseidea')}}"><i class="fa fa-stack-exchange" aria-hidden="true"></i> Idea category</a>
                    </li>
                    <li>
                        <a href="{{route('deptwiseallstudents')}}"><i class="fa fa-address-card"></i> All students</a>
                    </li>

                    {{--<li>--}}
                        {{--<a href="{{route('enable-post-idea')}}"><i class="fas fa-check"></i> Permit to post Idea</a>--}}
                    {{--</li>--}}

                    <li>
                        <a href="{{route('annomynousIdeaAndComment')}}"><i class="fa fa-user-secret"></i> Anonymous ideas and comments</a>
                    </li>

                    @elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('qualityassurancemanager'))

                    <li>
                        <a href="{{ route('posts.index') }}"><i class="fa fa-lightbulb-o" style="color: #ff5100;"></i> View all ideas</a>
                    </li>
                    <li>
                        <a href="{{route('no_idea_comments')}}"><i class="fa fa-user-secret" aria-hidden="true"></i> Ideas without any comments</a>
                    </li>

                    <li>
                        <a href="{{ route('tags.index') }}"><i class="fa fa-fw fa-edit"></i> Manage Tags</a>
                    </li>

                    <li>
                        <a href="{{ route('alldocs') }}"><i class="fa fa-file-text-o" aria-hidden="true"></i> All docs</a>
                    </li>
                    {{--<li>--}}
                        {{--<a href="{{ route('posts.index') }}"><i class="fa fa-lightbulb-o" aria-hidden="true"></i> View all ideas</a>--}}
                    {{--</li>--}}
                    @endif
                </ul>
            </div>

            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        @yield('content')
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
{{--    <script src="{{ asset('js/fontawesome-all.min.js') }}"></script>--}}
    {{--<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    
    @yield('script')
    <script>
        var arrayofID=[];
        $(document).ready(function () {

            $.ajax({
                url:'{{route('allnewPosts')}}',
                type:'get',
                success:function (data) {

                    $('#totalnewidea').text(data.length);
                    var list='';
                    for(var i=0;i<data.length;i++)
                    {
                        arrayofID.push(data[i].id);
                        list+='<li><a href="#"> <span class="glyphicon glyphicon-pencil" ></span> '+data[i].body+' by '+data[i].user.name+'</a></li>';
                    }
                    list+='<li><a href="{{route('approvenewidea')}}"><span class="glyphicon glyphicon-th"></span> View All</a> </li>';
                    $('#newideas').html(list);
                }
            })


            console.log(arrayofID);
        })
    </script>

</body>

</html>
