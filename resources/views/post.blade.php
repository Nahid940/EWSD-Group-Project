@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Post</div>
                <div class="panel-body">
                        <form method="post" action="{{ route('updatemyidea',['post' => $post->id]) }}" enctype="multipart/form-data">

                        {{ csrf_field() }}
                         {{-- {{ method_field('PATCH') }} --}}
                          <input type="hidden" name="user_id" id="user_id" class="form-control" value="{{ Auth::user()->id }}">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="tag_id">Tag Name</label>
                              <select class="form-control tag_id" name="tag_id" id="tag_id">
                                <option value="{{ $post->tag_id }}">{{ $post->tag->name }}</option>
                                @foreach ($tags as $tag)
                                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                @endforeach
                                </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label for="post_date">Date</label>
                            <div class="form-group">
                              <input type="date" name="post_date" id="post_date" class="form-control" value="{{ $post->created_at->format('Y-m-d') }}" required>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="title">Title</label>
                              <input type="text" name="title" id="title" class="form-control" value="{{ $post->title }}">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <label for="body">Your Idea</label>
                            <div class="form-group">
                              <textarea type="text" class="form-control" name="body">{{ $post->body }}</textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <label>Document</label>
                            <div class="form-group">
                              <input type="file" name="doc">
                              @if($post->document)
                              <label>Uploaded File: {{ $post->document->doc }}</label>
                              @endif
                            </div>
                        </div>
                          <div class="col-md-6">
                            <label>Privacy</label>
                            <div class="checkbox">
                              <label><input type="checkbox" value="0" name="profile_privacy"> Yes</label>
                              <label><input type="checkbox" value="1" name="profile_privacy"> No</label>
                            </div>
                          </div>

                        </div>


                         <button type="submit" class="btn btn-primary"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>

                        <a type="button" href="{{ route('myidea') }}" class="btn btn-danger"> <i class="glyphicon glyphicon-remove"> </i> Cancel</a>



                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

{{-- @section('script')
  <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
  <script>CKEDITOR.replace( 'body' );</script>
@endsection --}}