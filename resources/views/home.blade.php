@extends('layouts.app')

@section('style')
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">

  input.rounded {
  margin-right: 10px;
  border: 1px solid #ccc;
  -moz-border-radius: 20px;
  -webkit-border-radius: 20px;
  border-radius: 20px;
  -moz-box-shadow: 2px 2px 3px #666;
  -webkit-box-shadow: 2px 2px 3px #666;
  box-shadow: 2px 2px 3px #666;
  font-size: 18px;
  padding: 4px 7px;
  outline: 0;
  -webkit-appearance: none;
}

  .glyphicon.glyphicon-comment{
    font-size: 25px;
  }

  .glyphicon.glyphicon-thumbs-up, .glyphicon.glyphicon-thumbs-down{
    font-size: 25px;
  }
    a:link {
    text-decoration: none;
}
  a:hover {
    text-decoration: none;
}
  .ideaStyle{
      font-size: 16px;
      font-family: 'Slabo 27px', serif;
      font-weight:bold;
  }

    #idea{
        font-weight:bold;
    }
    .TotalideaLikeCount{
        display: none;
    }
      .TotalideaViewCount{
          display: none;
      }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            @if(session('alertMessage'))
                <div class="alert alert-success">
                    {{session('alertMessage')}}
                </div>
            @endif

            @if(session('commented'))
                <div class="alert alert-success">
                    <h4>{{session('commented')}}</h4>
                </div>
            @endif
            @if($postIdeaAndComment!=null)

            @if($postIdeaAndComment[0]['idea']=='yes')
            <div class="panel panel-primary">

                <div class="panel-heading">
                    Welcome to e-Idea | <i class="fa fa-lightbulb-o" aria-hidden="true" style="color: #fc5507;font-size: 30px"></i>  You can now post idea

                    <a class="btn btn-default pull-right" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                      Add New Idea
                    </a>

                    <div class="clearfix"></div>
                </div>

                <div class="collapse" id="collapseExample">
                    <div class="well">
                        <div class="modal-lg modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Submit New Idea</h4>
                              </div>
                              <div class="modal-body">
                                    <form id="createAjax" method="post" action="{{ url('post') }}" enctype="multipart/form-data">
                    
                                    {{ csrf_field() }}
                                      <input type="hidden" name="user_id" id="user_id" class="form-control" value="{{ Auth::user()->id }}"> 
                                    <div class="row">
                                      <div class="col-md-12">

                                          <div class="col-md-6">

                                              <div class="form-group">
                                                  <label for="tag_id">Tag Name</label>
                                                  <select class="form-control tag_id" name="tag_id" id="tag_id" required>
                                                      @foreach ($tags as $tag)
                                                          <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                                      @endforeach
                                                  </select>
                                              </div>
                                          </div>

                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label for="title">Title</label>
                                                  <input type="text" name="title" id="title" class="form-control" required>
                                              </div>
                                          </div>

                                          <div class="col-md-12">
                                              <label for="body">Your Idea</label>
                                              <div class="form-group">
                                                  <textarea type="text" class="form-control" id="body" name="body" required></textarea>
                                              </div>
                                          </div>


                                          <div class="col-md-6">
                                              <label>Document</label>
                                              <div class="form-group">
                                                  <input type="file" name="doc" class="form-control">
                                              </div>
                                          </div>


                                          <div class="col-md-6">
                                              <label>I want to hide my identity</label>
                                              <label class="radio-inline"><input type="radio" value="0" name="profile_privacy">Yes</label>
                                              <label class="radio-inline"><input type="radio" value="1" name="profile_privacy" required>No</label>

                                          </div>

                                          <div class="col-md-12">
                                              <p>Ideas will be published when it is approved by admin.<br> Admin has the right to remove your idea it it is not appropriate.</p>
                                              <label><input type="checkbox" id="termsaccept" value="" name="terms"> I accept terms and conditions.</label>
                                              <button type="submit" class="btn btn-primary  pull-right" id="submitbtn"> <i class="glyphicon glyphicon-ok"> </i>  Submit Idea</button>
                                          </div>
                                      </div>
                                    </div>

                                </form>
                              </div>
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
             
                    </div>
                </div>
                @endif

                @else
                    <div class="panel panel-primary">
                    <div class="panel-heading">
                        Welcome to e-Idea
                        <div class="clearfix"></div>
                    </div>
                @endif


                @php $x=0;@endphp
                <div class="panel-body">
                    @foreach ($posts as $post)
                        @php $x++;@endphp
                      <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                          <div class="modal-header">

                            <p class="lead">
                                Idea posted by @if($post->profile_privacy==1)
                                       {{$post->user->name}}
                                   @else
                                    <i class="fa fa-user-secret" aria-hidden="true" style="color: #ff3838;"></i> Anonymous
                                   @endif
                            </p>
                            <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $post->created_at->diffForHumans() }}
                            </p>

                          <div id="summary_area{{$post->id}}"><a href="javascript:void(0)" data-toggle="collapse" data-target="#comments_{{ $post->id }}"><b>{{sizeof($post->comment)}}</b> commments</a> | <a style="cursor: pointer;" id="totalLikes" class="dialogue" data-id="{{$x}}"><b>{{ \App\PostLike::where('post_id',$post->id)->where('likes','1')->count()}}</b> Likes</a> | <a style="cursor: pointer;" class="postViwers" data-id="{{$x}}"><b>{{sizeof($post->postViwers)}}</b> Viwers</a></div>

                          </div>
                          <div class="modal-body">
                            <p id="idea{{ $post->id }}" class="ideaStyle"><i class="fa fa-lightbulb-o" aria-hidden="true" style="color: #fc5507;font-size: 30px"></i> {{ str_limit($post->body,10) }} 
                            <a class="seeMore" href="javascript:void(0)" data-id="{{$post->id}}">See More</a>

                            </p>

                            <p class="postIdea ideaStyle" id="postIdea{{ $post->id }}"><i class="fa fa-lightbulb-o" aria-hidden="true" style="color: #fc5507;font-size: 30px"></i>{{ $post->body }}</p>
                            <a href="javascript:void(0)" data-toggle="collapse" data-target="#comments_{{ $post->id }}"><b>See all comments</b></a>
                          </div>


                          <div class="modal-footer">
                              <span id="message{{$post->id}}" class="messagespan" style="color: #ff2626;font-weight: bold"></span>

                              @php
                               $d= \App\PostLike::where('post_id',$post->id)->where('user_id',\Illuminate\Support\Facades\Auth::user()->id)->first();
                              @endphp

                                @if($d!=null)
                                        @if($d->likes=='1'  && $d->user_id==\Illuminate\Support\Facades\Auth::user()->id)
                                        <span id="likebutton{{$post->id}}">
                                            <a href="javascript:void(0)" class="thumbs_up" data-id="{{$post->id}}"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                        </span>

                                        <span id="dislikebutton{{$post->id}}">
                                            <a href="javascript:void(0)" class="thumbs_down" data-id="{{$post->id}}"><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                        </span>

                                        @elseif($d->dislike=='-1' && $d->user_id==\Illuminate\Support\Facades\Auth::user()->id)
                                          <span id="likebutton{{$post->id}}">
                                              <a href="javascript:void(0)" class="thumbs_up" data-id="{{$post->id}}"><span class="glyphicon glyphicon-thumbs-up"></span></a>
                                          </span>

                                          <span id="dislikebutton{{$post->id}}">
                                              <a href="javascript:void(0)" class="thumbs_down" data-id="{{$post->id}}"><i class="fa fa-thumbs-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                          </span>
                                        @endif
                                @else


                                  <span id="likebutton{{$post->id}}">
                                      <a href="javascript:void(0)" class="thumbs_up" data-id="{{$post->id}}"><i class="fa fa-thumbs-o-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                  </span>
                                  <span id="dislikebutton{{$post->id}}">
                                       <a href="javascript:void(0)" class="thumbs_down" data-id="{{$post->id}}"><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                  </span>

                                @endif

                            <span>
                              <a type="button" style="cursor: pointer;margin-right: 10px;margin-top: 8px;" class="glyphicon glyphicon-comment" title="Leave a Comments" data-toggle="collapse" data-target="#comments_{{ $post->id }}" aria-expanded="false" aria-controls="comments_{{ $post->id }}"></a>
                            </span>
                            
                            <div class="clearfix"></div>
             

                            <div class="collapse" id="comments_{{ $post->id }}">
                              <div class="well well-sm pull-left">
                                <div class="row">
                                  <div class="col-md-12">

                                      @if($postIdeaAndComment!=null)
                                          @if($postIdeaAndComment[0]['comment']=='yes')
                                            <form class="form-inline" method="post" action="{{ url('comment') }}">
                                             {{ csrf_field() }}
                                              <input type="hidden" name="post_id" value="{{ $post->id }}">
                                              <input type="hidden" name="user_id" value="{{ Auth::user()->id}}">
                                              <div class="form-group">
                                                <input size="60" name="comment" class="form-control rounded" type="text" placeholder=" Write a comment......" required>
                                              </div>
                                              <div style="margin-right: 10px;" class="checkbox">
                                                <label>
                                                  <input type="checkbox" name="anonymous" value="0"> Hide profile
                                                 {{--<input type="hidden" name="anonymous" value="0">--}}
                                                </label>
                                              </div>
                                              <button type="submit" class="btn btn-primary ">Comment</button>
                                            </form>
                                          @endif
                                      @endif

                                      <hr>
                                    <!-- Comment -->
                                    @foreach ($post->comment as $comment)
                                @if($comment->user->role->role_name=='student')
                                    <div class="media">
                                        <a class="pull-left" href="#">
                                            @if ($comment->anonymous == 1)
                                                <img class="media-object" src="{{asset('uploads/users/'.$post->user->image)}}" alt="" height="30px" width="30px">
                                            @else
                                                <img class="media-object" src="{{asset('image/anon.png')}}" alt="" height="30px" width="30px">
                                            @endif
                                        </a>
                                        <div class="media-body">
                                          <p style="text-align: left;">

                                              @if ($comment->anonymous == 1)
                                                  Comment by {{$comment->user->name}}
                                              @else
                                                   Comment by <i class="fa fa-user-secret" aria-hidden="true" style="color: #ff3838;"></i> Anonymous
                                              @endif
                                              <small>{{$comment->created_at->diffForHumans()}}</small><br>
                                                  <i class="fa fa-comments" aria-hidden="true" style="color: #56ccff"></i> {{ $comment->comment}}
                                          </p>
                                        {{--@endif--}}
                                          {{--</p>--}}
                                           
                                        </div>
                                    </div>
                                              @endif
                                     @endforeach
                                  </div>
                                </div>
                                <hr>
                              </div>

                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->

                        @if(sizeof($post->likes)>=1)
                        <div id="totalLikedialog{{$x}}" title="Peoples who liked this post" class="TotalideaLikeCount">

                                @foreach($post->likes as $lks)
                                    @if($lks->likes=='1')
                                        <li style="list-style: none"><i class="fa fa-thumbs-up" aria-hidden="true" style="color: #38cdff;"></i> {{$lks->user->name}} liked this.</li>
                                        @else
                                        <li style="list-style: none"> <i class="fa fa-thumbs-down" aria-hidden="true" style="color: #38cdff;"></i> {{$lks->user->name}} disliked this.</li>
                                    @endif
                                @endforeach
                        </div>
                        @else
                            <div id="totalLikedialog{{$x}}" title="Peoples who liked this post" class="TotalideaLikeCount">
                                <p>No one liked yet !!</p>
                            </div>
                        @endif


                    @php
                    $datas=\App\PostViwer::with('user')->where('post_id',$post->id)->groupBy('user_id')->get();

                    @endphp

                        @if(sizeof($post->postViwers)>=1)
                            <div id="postViwers{{$x}}" title="Peoples viewed this post" class="TotalideaViewCount">
                                @foreach($datas as $s)
                                    <li style="list-style: none"><i class="fa fa-eye" aria-hidden="true" style="color: #38cdff;"></i> {{$s->user->name}} viewed this.</li>
                                @endforeach
                            </div>
                        @else
                            <div id="postViwers{{$x}}" title="Peoples who viewed this post" class="TotalideaViewCount">
                                <p>No view !!</p>
                            </div>
                        @endif

                      @endforeach

                    {{$posts->links()}}

                </div>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
      $(document).ready(function () {
        $("#submitbtn").attr("disabled", "disabled");
        $('.postIdea').hide();
      });


      
        $(document).on('click','.seeMore',function () {
          var postid=($(this).data('id'));
          $.ajax({
              url: "{{ route('postView') }}",
              type: 'POST',
              data:{
                    _token:'{{csrf_token()}}',
                    post_id:$(this).data('id'),
                    user_id:'{{\Illuminate\Support\Facades\Auth::user()->id}}',
              },
              success: function (result) {
                 console.log(result);
                //var replyObject = JSON.parse(result);
                $('#idea'+postid).hide();
                $('#postIdea'+postid).toggle();
                  $('#summary_area'+postid).load(location.href +' #summary_area'+postid);
              }
          });
          
        });  
        
       


        $(document).on('click','#termsaccept',function () {
            if ($('#termsaccept').is(':checked')) {
                $('#termsaccept').val("yes");
                $("#submitbtn").attr("disabled", false);
            }else{
                $('#termsaccept').val("not");
                $("#submitbtn").attr("disabled", true);
            }
        });

        $(document).on('click','.thumbs_up',function () {
            var postid=($(this).data('id'));


            $.ajax({
                type:'post',
                url:'{{route('postLike')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    post_id:$(this).data('id'),
                    user_id:'{{\Illuminate\Support\Facades\Auth::user()->id}}',
                    like:'1',
                    dislike:'0',
                    Buttonaction:'like'
                },
                success:function (data) {
                    if(data=='done')
                    {
                        $('#message'+postid).fadeIn();
                        $('#message'+postid).text('You have already liked this post !!');
                        setTimeout(function() {
                            $('#message'+postid).fadeOut('slow');
                        }, 2000);

                    }else if(data['newreocrdadded']){
                        $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                        $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'"><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                    }else{
                        $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"><i class="fa fa-thumbs-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                        $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'" ><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                    }

                    $('#summary_area'+postid).load(location.href +' #summary_area'+postid);
                },
                error:function (err) {
                    alert(err.responseText)
                }
            })
        })
        $(document).on('click','.thumbs_down',function () {
            var postid=($(this).data('id'));
            $.ajax({
                type:'post',
                url:'{{route('postLike')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    post_id:$(this).data('id'),
                    user_id:'{{\Illuminate\Support\Facades\Auth::user()->id}}',
                    like:'0',
                    dislike:'-1',
                    Buttonaction:'dislike'
                },
                success:function (data) {
                    if(data=='done')
                    {
                        $('#message'+postid).show();
                        $('#message'+postid).text('You have already disliked this post !!');
                        setTimeout(function() {
                            $('#message'+postid).hide('slow');
                        }, 3000);

                    }else if(data['newreocrdadded']=='ok'){

                        $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"  ><i class="fa fa-thumbs-o-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                        $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'" ><i class="fa fa-thumbs-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                    }else{
                        $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"><i class="fa fa-thumbs-o-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                        $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'"><i class="fa fa-thumbs-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                    }
                    $('#summary_area'+postid).load(location.href +' #summary_area'+postid);
                },
                error:function (err) {
                    alert(err.responseText)
                }
            })
        })



        $(document).on('click','.dialogue',function () {
            var x=$(this).data('id');
            $( "#totalLikedialog"+x ).dialog();
        })

      $(document).on('click','.postViwers',function () {
            var x=$(this).data('id');
            $( "#postViwers"+x ).dialog();
        })


    </script>
@endsection


