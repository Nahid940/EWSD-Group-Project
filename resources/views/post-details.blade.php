@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Post Details</div>

                <div class="panel-body">
        
                
                        <h2>
                            <a href="#">{{ $post->title }}</a>
                        </h2>
                        <p class="lead">
                            by <a href="index.php">{{ $post->user->name }}</a>
                        </p>
                        <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $post->created_at }}</p>
                        <hr>
                        <p>{{ $post->body }}</p>
                        

                        <hr>
                
     
 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

