@extends('layouts.admin')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.0/semantic.min.css" rel="stylesheet" type="text/css">
    <link href=" https://cdn.datatables.net/1.10.16/css/dataTables.semanticui.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12" style="margin-top: 50px">
            <h3>All Students</h3>

            <table class="ui celled table" cellspacing="0" width="100%" id="myTable">

                <thead>
                    <tr>
                        <th>Student name</th>
                        <th>Department</th>
                        <th>Session</th>
                        <th>Batch</th>
                        <th>Email</th>
                        <th>Phone</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($data as $d)
                        <tr>
                            <td>{{$d->user->name}}</td>
                            <td>
                                @if($d->department!=null){{$d->department->name}}

                                @else
                                    No department found
                                @endif
                            </td>
                            <td> @if($d->session!=null)
                                    {{$d->session->name}}
                                 @else
                                    No Session
                                 @endif
                            </td>
                            <td>@if($d->batch!=null)
                                    {{$d->batch->name}}
                                    @else
                                No batch
                                    @endif
                            </td>
                            <td>{{$d->user->email}}</td>
                            <td>{{$d->user->phone}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>


@endsection

@section('script')

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src=" https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="  https://cdn.datatables.net/1.10.16/js/dataTables.semanticui.min.js"></script>
    <script src="  https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.0/semantic.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#myTable').DataTable();
        });
    </script>
@endsection