@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">USERS</span>
        <button id="btnCreate" type="button" class="btn btn-default pull-right hidden-print" data-toggle="modal" data-target="#createModal"><i class="glyphicon glyphicon-plus"> </i> Add New User</button>
        <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        <button id="btnRefresh" class="btn btn-success pull-right hidden-print" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></button>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">

        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th> Name</th>
                    <th> Email</th>
                    <th> Image</th>
                    <th> Department</th>
                    <th class="hidden-print">Action</th>
                </tr>
            </thead>
            <tbody id="userRows">
                
            </tbody>
        </table>
      </div>
    </div>

    <!--Create Modal-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form id="createAjax" enctype="multipart/form-data">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Add New User</h4>
          </div>
          <div class="modal-body">
                {{ csrf_field() }}
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="role_id">Role</label>
                              <select class="form-control role_id" name="role_id" id="role_id" required>
                              </select>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label for="name">Name</label>
                              <input type="text" name="name" id="name" class="form-control" required>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <label for="address">Address</label>
                          <div class="form-group">
                              <textarea type="text" cols="3" rows="3" class="form-control" name="address" required></textarea>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <label for="phone">Phone</label>
                          <div class="form-group">
                              <input type="text" name="phone" id="phone" class="form-control" required>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <label for="email">Email</label>
                          <div class="form-group">
                              <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email" required>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <label for="image">Image</label>
                          <div class="form-group">
                              <input type="file" name="image" class="form-control" id="image" required>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <label for="password">Password</label>
                          <div class="form-group">
                              <input type="password" name="password" id="password" class="form-control" required>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <label for="status">Status</label>
                          <div class="form-group">
                              <select class="form-control" name="status" required>
                                  <option value="0">Active</option>
                                  <option value="1">Inactive</option>
                              </select>
                          </div>
                      </div>
                  </div>

              <div class="row" id="studentinfo">

              </div>

              <div class="row" id="studentinfo1">

              </div>

              <input type="hidden" name="role_name_input" id="role_name_input">


          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary"> <i class="glyphicon glyphicon-ok"> </i>  Save</button>   
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--Edit Modal-->

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form class="editAjax" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="heading">Edit User</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <span style="float: left;"  id="editimageHolder"></span>
                        </div>
                        <input type="hidden" id="userID" name="id">

                        <div id="alertMessageContainer" class="alert alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <em id="alertMessage"></em>
                        </div>

                        {{ method_field('PATCH') }}
                        {{--  <input type="hidden" name="_token" value=""> --}}
                        <div class="form-group">
                            <select class="form-control role_id" name="role_id" id="edit_role_id">
                                <option value="">Role</option>
                                <option value="1">Admin</option>
                                <option value="2">Student</option>
                                <option value="3">QA Coordinator</option>
                                <option value="4">QA Manger</option>
                            </select>
                        </div>

                        <label for="name">Name</label>
                        <div class="form-group">
                            <input type="text" name="name" id="edit_name" class="form-control" required>
                        </div>
                        <label for="address">Address</label>
                        <div class="form-group">
                            <input type="text"  class="form-control" name="address" id="edit_address" required>
                        </div>
                        <label for="phone">Phone</label>
                        <div class="form-group">
                            <input type="text" name="phone" id="edit_phone" class="form-control" required>
                        </div>
                        <label for="email">Email</label>
                        <div class="form-group">
                            <input type="email" name="email" id="edit_email" class="form-control" placeholder="Enter Email" required>
                        </div>
                        <label for="image">Image</label>
                        <div class="form-group">
                            <input type="file" name="image" class="form-control" id="edit_image">
                        </div>

                        <label for="status">Status</label>
                        <div class="form-group">
                            <select class="form-control" name="status" id="edit_status">
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnUpdateUser"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>
                </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->





    <!--Show Modal-->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">User Details</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6">
                <span style="float: left;"  id="imageHolder"></span>
              </div>
              <div class="col-sm-6">
                <p id="body"></p>
              </div>
            </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--Message Modal-->
    <div class="modal fade" id="deleteConfirmationModel" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p id="body">Are You Sure to delete?</p>
          </div>
          <div class="modal-footer">
              <input type="hidden" id="deleteUserId">
            <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>            
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
@endsection

@section('script')

    <script>
        $(document).ready(function() {
            $('#alertMessageContainer').css({'display':'none'});
            $('#btnRefresh').on('click', function() {
                loadUser();
            });

            loadUser();

            function loadUser() {
                $.ajax({
                    url: "{{ route('users.index') }}",
                    type: 'GET',
                    success: function(users) {
                        var currentUsers = (users);

//                        console.log(currentUsers.);
//                        console.log(currentUsers);
                        var resultContainer = $('#userRows');
                        var rows = '', showUrl = '', token = '', number = 1;
                        currentUsers.map(function(user) {
                            showUrl = ' {{ route('users.show', ['user' => ':userId']) }}';
                            showUrl = showUrl.replace(':userId', user.id);

//                            console.log(user.id);

                            rows += '<tr>' +
                                    '<td>' + number++ + '</td>' +
                                    '<td>' + user.name + '</td>' +
                                    '<td>' + user.email + '</td>' +
                                    '<td><img width="60" height="60" src="/uploads/users/'+user.image+'"></td>' +
                                    '<td>' +
                                    '<button type="button" class="btnShow hidden-print btn btn-success glyphicon glyphicon-eye-open" id="show" title="Show"  data-id="'+user.id+'"></button>' + ' '+
                                    '<button type="button" class="btnEdit hidden-print btn btn-warning glyphicon glyphicon-edit" id="edit" title="Edit" data-id="'+user.id+'"></button>' + ' ' +
                                    '<button type="button" class="btnDelete hidden-print btn btn-danger glyphicon glyphicon-trash" id="delete" title="Delete" data-id="'+user.id+'"></button>'
                                    + '</td>' +
                                    '</tr>'
                        });
                        resultContainer.html(rows);
//                        $('.btnShow').on('click', openShowModal);
//                        $('.btnEdit').on('click', openEditModal);
                        $('.btnDelete').on('click', showDeleteConfirmation);
                    }
                });
            } 


            $('#btnCreate').on('click', function() {
              $.ajax({
                  url: "{{ route('users.create') }}",
                  type: 'GET',
                 // data: $("#createAjax").serialize(),
                  //console.log(replyObject);
                  success: function (results) {
                    console.log(results);
                    var currentRoles = results;
                    var resultContainer = $('.role_id');
                    var option = "";
                    //var options = "";
                      option+="<option value=''>Role</option>";
                    currentRoles.map(function(result) {
                        //console.log(result);
                      option += "<option value='" + result.id +"' data-id='"+result.role_name+"'>"+ result.designation +"</option>";
                    });
                    resultContainer.html(option);
                  }
              });
            });


                $('#createAjax').on('submit', function(e)  {

                    e.preventDefault();
                    $.ajax({
                        url: "{{ route('users.store') }}",
                        type: 'POST',
                        data:  new FormData(this),
                        contentType: false,
                        processData:false,
                        success: function (result) {
//                            console.log(result);
                        var replyObject = JSON.parse(result);
                        //console.log(replyObject.alertType);
                        $('#alertMessageContainer').css({'display':'block'});

                        $('#alertMessageContainer').addClass(replyObject.alertType);
                        $('#alertMessage').html(replyObject.alertMessage);
                        $('alertMessageContainer').show();
                        $('#createAjax')[0].reset();
                        loadUser();
                        $('#createModal').modal('hide');
                        },

                        error: function (jqXHR) {
                            alert(jqXHR.responseText);
                        }

                    });
                });



            $(document).on('click','#show',function () {
                var id=($(this).data('id'));

                $.ajax({
                    url: '{{ route('users.show', ['user' => 'id']) }}',
                    type: 'get',
                    data:{
                        _token:'{{csrf_token()}}',
                        id:id
                    },

                    success: function(result) {

                        var status = '';

                        if(result[1]=='student'){
                            $('#body').html([
                                'Role:  Student <br>',
                                'Name: ' + result[0].user.name + '<br>',
                                'Email: ' + result[0].user.email + '<br>',
                                'Address: ' + result[0].user.address + '<br>',
                                'Phone: ' + result[0].user.phone + '<br>',
                                'Department: ' + result[0].department.name + '<br>',
                                'Status: ' + status + ' <br>',
                            ]);

                            $('#imageHolder').html([
                                '<img width="250" height="250" src="/uploads/users/'+result[0].user.image+'">'
                            ]);
                        }else if(result[1]=='qacoordinator')
                        {
                            $('#body').html([
                                'Role:  Qa coordinator <br>',
                                'Name: ' + result[0].user.name + '<br>',
                                'Email: ' + result[0].user.email + '<br>',
                                'Address: ' + result[0].user.address + '<br>',
                                'Phone: ' + result[0].user.phone + '<br>',
                                'Department: ' + result[0].department.name + '<br>',
                                'Status: ' + status + ' <br>',
                            ]);
                            $('#imageHolder').html([
                                '<img width="250" height="250" src="/uploads/users/'+result[0].user.image+'">'
                            ]);
                        }else{
                            $('#body').html([
                                'Role:  '+result.role.designation+' <br>',
                                'Name: ' + result.name + '<br>',
                                'Email: ' + result.email + '<br>',
                                'Address: ' + result.address + '<br>',
                                'Phone: ' + result.phone + '<br>',
                                'Password: ' + result.password + '<br>',
                                'Status: ' + status + ' <br>',
                            ]);

                            $('#imageHolder').html([
                                '<img width="250" height="250" src="/uploads/users/'+result.image+'">'
                            ]);
                        }

                        if(result.status == 0) {
                            status =  '<span class="label label-success">Active</span>';
                        }else{
                            status =  '<span class="label label-danger">Inactive</span>';
                        }


                    }
                });
                $('#showModal').modal();
            });



            $(document).on('click','#edit',function () {
                var id=($(this).data('id'));
                var url= '{{ route('users.edit',":id") }}';
                $.ajax({

                 url : url.replace(':id',id),
                    type: 'get',
                    data:{
                        _token:'{{csrf_token()}}',
                        id:id
                    },
                    success: function(result) {
//                        console.log(result);
                        $('#userID').val(result.id);
                        $('#edit_name').val(result.name);
                        $('#edit_address').val(result.address);
                        $('#edit_phone').val(result.phone);
                        $('#edit_email').val(result.email);
                        $('#edit_status').val(result.status);
                        $('#edit_role_id').val(result.role.id);
                        $('#editimageHolder').html([
                            '<img width="100" height="100" src="/uploads/users/'+result.image+'">'
                        ]);
                    },
                    error:function (ext) {
                        alert(ext.responseText);
                    }
                });
                $('#editModal').modal();
            });


            $('#btnUpdateUser').on('click', updateUser)
            ;
            function showDeleteConfirmation() {
                var deleteUserId = $(this).val();
                $('#deleteButton').val(deleteUserId);
                $('#deleteConfirmationModel').modal();
            }

            $(document).on('click','.btnDelete',function () {
                $('#deleteUserId').val($(this).data('id'));
            })

          $('#deleteButton').on('click', deleteUser);
           function deleteUser() {
               var deleteableUserId = $('#deleteUserId').val();
               var deleteUrl = '{{ route('users.destroy', ['user' => ':userId']) }}';
               deleteUrl = deleteUrl.replace(':userId', deleteableUserId);

               $.ajax({
                   url : deleteUrl,
                   type : 'POST',
                   data : {
                       "_token": '{{ csrf_token() }}',
                       "_method": 'DELETE'
                   },
                   success: function(result) {
                       loadUser();
                       $('#deleteConfirmationModel').modal('hide');
                   },
                   error:function (ext) {
                       alert(ext.responseText);
                   }
               });
           }


            function updateUser(e) {
               e.preventDefault();
                var id = $('#userID').val();
                var updateUrl = '{{ route('users.update', ['user' => ':userId']) }}';
                updateUrl = updateUrl.replace(':userId', id);
                var formData =new FormData($('.editAjax')[0]);
                $.ajax({
                    url: updateUrl,
                    type: 'post',
                    data: formData,
                    contentType: false,
                    processData:false,
                    success: function(result) {
//                        console.log(result);
                        if(result=='updated')
                        {
                            loadUser();
                            $('#editModal').modal('hide');
                        }
                    },
                    error:function (er) {
                        alert(er.responseText);
                    }
                });
            }

        });



        $(document).on('change','#role_id',function () {
            //if student// in my db student id value is 1
            var role_name=$('#role_id').find(':selected').attr('data-id');
            $('#role_name_input').val(role_name);
            if(role_name=='student') {

                $.ajax({
                    url: "{{ route('departments.index') }}",
                    type: 'GET',
                    success: function(departments) {
                        var currentDepartments = JSON.parse(departments);
                        var resultContainer = $('#departments');
                        var option = '', showUrl = '', token = '', number = 1;
                        option+="<option value=''>Select department</option>";
                        currentDepartments.map(function(department) {
                            option+="<option value="+department.id+">"+department.name+"</option>";
                        });
                        resultContainer.html(option);
                    }
                });

                $('#studentinfo').html(
                    '<div class="col-md-6"><label for="">Department</label><select class="form-control" id="departments" name="department_id" required></select></div>' +
                    '<div class="col-md-6"><label for="">Session</label><select class="form-control" id="sessions" name="session_id" required></select></div>'
                )
                $('#studentinfo1').html(
                    '<div class="col-md-6"><label for="">Batch</label><select class="form-control" id="batches" name="batch_id" required></select></div>'
                )
            }else if(role_name=='qacoordinator'){
                $('#studentinfo').html("");
                $('#studentinfo1').html("");


                $.ajax({
                    url: "{{ route('departments.index') }}",
                    type: 'GET',
                    success: function(departments) {
                        var currentDepartments = JSON.parse(departments);
                        var resultContainer = $('#departments');
                        var option = '', showUrl = '', token = '', number = 1;
                        option+="<option value=''>Select department</option>";
                        currentDepartments.map(function(department) {
                            option+="<option value="+department.id+">"+department.name+"</option>";
                        });
                        resultContainer.html(option);
                    }
                });

                $('#studentinfo').html(
                    '<div class="col-md-6"><label for="">Department</label><select class="form-control" id="departments" name="department_id"></select></div>'
                )
            }else{
                $('#studentinfo').html("");
                $('#studentinfo1').html("");
            }
        });

        $(document).on('change','#departments',function () {

            var id=$('#departments').val();
            $.ajax({
                url: '{{ route('getDeptWiseSession') }}',
                type: 'get',
                data:{
                    _token:'{{csrf_token()}}',
                    id:id
                },
                success: function(result) {
                    var currentDepartments = JSON.parse(result);
                    var resultContainer = $('#sessions');
                    var option = '', showUrl = '', token = '', number = 1;
                    option+="<option value=''>Select department</option>";
                    currentDepartments.map(function(sessions) {
                        option+="<option value="+sessions.id+">"+sessions.name+"</option>";
                    });
                    resultContainer.html(option);
                },
                error:function (er) {
                    alert(er.response);
                }
            })
        })


        $(document).on('change','#sessions',function () {

            var id=$('#sessions').val();
            $.ajax({
                url: '{{ route('getSessionWiseBatch') }}',
                type: 'get',
                data:{
                    _token:'{{csrf_token()}}',
                    id:id
                },
                success: function(result) {
                    var currentDepartments = JSON.parse(result);
                    var resultContainer = $('#batches');
                    var option = '', showUrl = '', token = '', number = 1;
                    option+="<option value=''>Select batch</option>";
                    currentDepartments.map(function(sessions) {
                        option+="<option value="+sessions.id+">"+sessions.name+"</option>";
                    });
                    resultContainer.html(option);
                }
            })

        })
    </script>

@endsection