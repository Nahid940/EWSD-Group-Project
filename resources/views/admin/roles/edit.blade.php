@extends('layouts.admin')

@section('content')


	<div class="panel panel-primary panel-top">    
	  <div class="panel-heading">
	    <span class="heading">Edit Role</span>
	     <a type="button" href="{{ route('roles.index') }}" class="btn btn-danger pull-right"> <i class="glyphicon glyphicon-remove"> </i> Cancel</a>
	      <div class="clearfix"></div>
	  </div>
	  <div class="panel-body">
      @if ($message = Session::get('update'))
      <div class="alert alert-success alert-block">
         <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
      </div>
      @endif

      @if(count($errors))
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.
            <br/>
            <ul>
              @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

	        <form method="POST" action="{{ route('roles.update', ['role' => $role->id]) }}">
	          {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <label for="designation">Designation</label>
            <div class="form-group">
              <input type="text" name="designation" id="designation" class="form-control" placeholder="Enter Designation" value="{{ $role->designation }}" required>
            </div>
            <label for="salary">Salary</label>
            <div class="form-group">
              <input type="text" name="salary" id="salary" class="form-control" placeholder="Enter Salary" value="{{ $role->salary }}">
            </div>
            <br>
            <button type="submit" class="btn btn-primary m-t-15 waves-effect"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>
        </form>
	  </div>
	</div>
  
@endsection