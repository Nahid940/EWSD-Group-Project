@extends('layouts.admin')

@section('content')


	<div class="panel panel-primary panel-top">
	  <div class="panel-heading">
	    <span class="heading">Add New Role</span>
	     <a type="button" href="{{ route('roles.index') }}" class="btn btn-danger pull-right"> <i class="glyphicon glyphicon-remove"> </i> Cancel</a>
	      <div class="clearfix"></div>
	  </div>
	  <div class="panel-body">
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
         <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
      </div>
      @endif
       @if(count($errors))
           <div class="alert alert-danger">
             <strong>Whoops!</strong> There were some problems with your input.
             <br/>
             <ul>
               @foreach($errors->all() as $error)
               <li>{{ $error }}</li>
               @endforeach
             </ul>
           </div>
         @endif
         
	        <form method="POST" action="{{ route('roles.store') }}">
	        {{ csrf_field() }}
            <label for="designation">Designation</label>
            <div class="form-group">
              <input type="text" name="designation" id="designation" class="form-control" placeholder="Enter Designation" required>
            </div>    
            <label for="salary">Salary</label>
            <div class="form-group">
              <input type="text" name="salary" id="salary" class="form-control" placeholder="Enter Salary" required>
            </div>          
            <br>
            <button type="submit" class="btn btn-primary"> <i class="glyphicon glyphicon-ok"> </i>  Save</button>
        </form>
	  </div>
	</div>
  
@endsection