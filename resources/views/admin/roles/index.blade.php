@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">ROLE</span>
        <a type="button" href="{{route('roles.create')}}" class="btn btn-default pull-right hidden-print"> <i class="glyphicon glyphicon-plus"> </i> Add New Role</a>
        <button style="margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        @if ($message = Session::get('delete'))
        <div class="alert alert-danger alert-block">
           <button type="button" class="close" data-dismiss="alert">×</button> 
               <strong>{{ $message }}</strong>
        </div>
        @endif
        @if ($roles->count())
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="70%">Designation</th>
                    <th class="hidden-print">Action</th>
                </tr>
            </thead>
            @foreach($roles as $role)
            <tbody>
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $role->designation }}</td>
                    <td class="hidden-print">
                        <a type="button" href="{{ route('roles.show', ['role' => $role->id]) }}" class="btn btn-success glyphicon glyphicon-eye-open" title="Show"></a>
                        <a type="button" href="{{ route('roles.edit', ['role' => $role->id]) }}" class="btn btn-warning glyphicon glyphicon-edit" title="Edit"></a>
                        <form action="{!! action('RoleController@destroy', $role->id) !!}" method="POST" style="display: inline-block;">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                           <button type="submit" title="Delete" role="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash" title="Delete"></i></button>
                        </form>        
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
        @endif
      </div>
    </div>

@endsection