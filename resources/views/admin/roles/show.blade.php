@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">ROLE DETAILS</span>
        <a type="button" href="{{route('roles.index')}}" class="btn btn-default pull-right hidden-print"> <i class="glyphicon glyphicon-menu-left"> </i> Go To Index</a>
        <button style="margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <p><label>Designation:</label> {{ $role->designation }}</p>
        <p><label>Salary:</label> {{ $role->salary }}</p>
 
        <div class="row">
            <div class="col-lg-12 text-right hidden-print">
                  <a type="button" href="{{ route('roles.edit', ['role' => $role->id]) }}" class="btn btn-success glyphicon glyphicon-edit" title="Edit"></a>
                <form action="{!! action('RoleController@destroy', $role->id) !!}" method="POST" style="display: inline-block;">
                    {{ csrf_field() }} {{ method_field('DELETE') }}
                   <button type="submit" title="Delete" role="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash" title="Delete"></i></button>
                </form>        
            </div>
        </div>
      </div>
    </div>
    
@endsection