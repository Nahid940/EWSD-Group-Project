@extends('layouts.admin')

@section('content')

    <div class="col-md-12">
        <div class="col-md-6">
            <div class="panel panel-primary panel-top">
                <div class="panel-heading">
                    <span class="heading">TAG</span>
                    <button type="button" class="btn btn-default pull-right hidden-print" data-toggle="modal" data-target="#createModal"><i class="glyphicon glyphicon-plus"> </i> Add New Tag</button>
                    <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
                    <button id="btnRefresh" class="btn btn-success pull-right hidden-print" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></button>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tag Name</th>
                            <th class="hidden-print">Action</th>
                        </tr>
                        </thead>
                        <tbody id="tagRows">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">

            <div class="panel panel-primary">
                <div class="panel-heading"><h4>Tags used by students</h4></div>
                <div class="panel-body">

                    <table class="table">

                        <thead>
                            <th>#</th>
                            <th>Tag name</th>
                            <th>Total used</th>
                        </thead>

                        @php
                        $i=1;
                        @endphp

                        <tbody>
                            @foreach($contributor as $con)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$con->tagname}}</td>
                                    <td>{{$con->totaltag}} times</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <!--Create Modal-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form class="createAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Add New Department</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
               {{--  <input type="hidden" name="_token" value=""> --}}
                <label for="name">Tag Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Department Name" required>
                </div>         
                <br>                  
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary"> <i class="glyphicon glyphicon-ok"> </i>  Save</button>   
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--Show Modal-->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Department Details</h4>
          </div>
          <div class="modal-body">
            <p id="body"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!--Edit Modal-->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form class="editAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Edit Department</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
               {{--  <input type="hidden" name="_token" value=""> --}}
                <label for="name"> Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Department Name" required>
                </div>    
                      
                <br>                  
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btnUpdateTag"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>   
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    

    <!--Message Modal-->
    <div class="modal fade" id="deleteConfirmationModel" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p id="body">Are You Sure to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>            
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('script')

    <script>
        $(document).ready(function() {
            $('#alertMessageContainer').css({'display':'none'});
            $('#btnRefresh').on('click', function() {
            loadTag();
            });

            loadTag();

            function loadTag() {
                $.ajax({
                    url: "{{ route('tags.index') }}",
                    type: 'GET',
                    success: function(tags) {
                        var currentTags = JSON.parse(tags);
                        var resultContainer = $('#tagRows');
                        var rows = '', showUrl = '', token = '', number = 1;
                        currentTags.map(function(tag) {
                            showUrl = ' {{ route('tags.show', ['tag' => ':tagId']) }}';
                            showUrl = showUrl.replace(':tagId', tag.id);
                            
                            rows += '<tr>' +
                                    '<td>' + number++ + '</td>' +
                                    '<td>' + tag.name + '</td>' +
                                    '<td>' + 
                                    '<button type="button" class="btnShow hidden-print btn btn-success glyphicon glyphicon-eye-open" title="Show" value="'+ tag.id +'"></button>' + ' '+
                                    '<button type="button" class="btnEdit hidden-print btn btn-warning glyphicon glyphicon-edit" title="Edit" value="'+ tag.id +'"></button>' + ' ' +
                                    '<button type="button" class="btnDelete hidden-print btn btn-danger glyphicon glyphicon-trash" title="Delete" value="'+ tag.id +'"></button>' 
                                    + '</td>' +
                                    '</tr>'
                        });
                        resultContainer.html(rows);
                        $('.btnShow').on('click', openShowModal);
                        $('.btnEdit').on('click', openEditModal);
                        $('.btnDelete').on('click', showDeleteConfirmation);
                    }
                });
            } 


             $('.createAjax').on('submit', function(e)  {
                e.preventDefault();
                
                $.ajax({
                    url: "{{ route('tags.store') }}",
                    type: 'POST',
                    data: $(".createAjax").serialize(),
                    //console.log(replyObject);
                    success: function (result) {
                       // console.log(result);
                        var replyObject = JSON.parse(result);
                        //console.log(replyObject.alertType);
                        $('#alertMessageContainer').css({'display':'block'});

                        $('#alertMessageContainer').addClass(replyObject.alertType);
                        $('#alertMessage').html(replyObject.alertMessage);
                        $('alertMessageContainer').show();
                        $('.createAjax')[0].reset();
                        loadTag();
                        $('#createModal').modal('hide');
                    }
                });
            });


            function openShowModal() {
                var showableTagId = $(this).val();
                showUrl = ' {{ route('tags.show', ['tag' => ':tagId']) }}';
                showUrl = showUrl.replace(':tagId', showableTagId);
                $.ajax({
                    url: showUrl,
                    type: 'GET',
                    success: function(result) {
                        var replyObject = JSON.parse(result);
                        $('#body').html([
                            'Tag Name: ' + replyObject.name + '<br>',
                        ]);
                    }
                });
                $('#showModal').modal();
            }


            function openEditModal() {
                  var editableTagId = $(this).val();
                  var showUrl = '{{ route('tags.edit', ['tag' => ':tagId']) }}';
                  showUrl = showUrl.replace(':tagId', editableTagId);

                  $.ajax({
                      url: showUrl,
                      type: 'GET',
                      success: function(result) {
                          var replyObject = JSON.parse(result);
                          $('.editAjax #name').val(replyObject.name);

                          $('#btnUpdateTag').val(editableTagId);
                          $('#editModal').modal();
                      }
                  });
              }


               function updateTag() {
                  var updateableTagId = $(this).val();
                  var updateUrl = '{{ route('tags.update', ['tag' => ':tagId']) }}';
                  updateUrl = updateUrl.replace(':tagId', updateableTagId);
                  var formData = $("form.editAjax").serialize() + "&_method=PATCH";
                  //console.log(formData);
                  $.ajax({
                      url: updateUrl,
                      type: 'POST',
                      data: formData,
                      success: function(result) {
                          console.log(result);  
                          loadTag();                    
                      }
                  });
              }

            $('#btnUpdateTag').on('click', updateTag);

            function showDeleteConfirmation() {
                 var deleteTagId = $(this).val();
                 $('#deleteButton').val(deleteTagId);
                 $('#deleteConfirmationModel').modal();
             }

            $('#deleteButton').on('click', deleteTag);

            function deleteTag() {
                 var deleteableTagId = $(this).val();
                 var deleteUrl = '{{ route('tags.destroy', ['tag' => ':tagId']) }}';
                 deleteUrl = deleteUrl.replace(':tagId', deleteableTagId);

                 $.ajax({
                     url : deleteUrl,
                     type : 'POST',
                     data : {
                         "_token": '{{ csrf_token() }}',
                         "_method": 'DELETE'
                     },
                     success: function(result) {
                         loadTag();
                         $('#deleteConfirmationModel').modal('hide');
                     }
                 });

             }

           
        });
    </script>

@endsection