@extends('layouts.admin')

@section('content')
	<h1 class="page-header">
	    Your Dashboard
	</h1>


	@if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-3">
				<div class="panel panel-success">
					<div class="panel-heading">Total Staff</div>
					<div class="panel-body">
						<h3><span class="label label-success">{{$staff}}</span> Staffs</h3>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading">Total Students</div>
					<div class="panel-body">
						<h3><span class="label label-primary">{{$student}}</span> Students</h3>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-info">
					<div class="panel-heading">Total Department</div>
					<div class="panel-body">
						<h3><span class="label label-info">{{$dept}}</span> Departments</h3>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="panel panel-success">
					<div class="panel-heading">Total Batch</div>
					<div class="panel-body">
						<h3><span class="label label-success">{{$dept}}</span> Batches</h3>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="col-md-6" >
				<h4>Total student of each department</h4>
				<canvas id="totalstudent"></canvas>
			</div>

		</div>
	</div>

	@elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('qacoordinator'))
		<div class="row">
			@if($totalstudent!=null && $myDept!=null && $session!=null)

				<div class="col-md-12">
					<div class="alert alert-info">Your department <b>{{$myDept->department->name}}</b></div>
					<div class="col-md-6">
						<div class="panel panel-success">
							<div class="panel-heading">Total Students of Your Department</div>
							<div class="panel-body">
								<h3><span class="label label-success"></span>{{$totalstudent}} Students</h3>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-success">
							<div class="panel-heading">Total Sessions of Your Department</div>
							<div class="panel-body">
								<h3><span class="label label-success"></span>{{$session}} Session</h3>
							</div>
						</div>
					</div>
				</div>
			@endif
		</div>

	@elseif(\Illuminate\Support\Facades\Auth::user()->hasRole('qualityassurancemanager'))
		<div class="row">
			<div class="col-md-12">
					<div class="col-md-6" >
						<h4>Total idea of each department</h4>
						<canvas id="totalIdeaofEachDept"></canvas>
					</div>


					<div class="col-md-6">
						<h4>Idea percentage of each department:</h4>
						<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Department</th>
									<th>Percentage of idea post  (<i class="fas fa-percent"></i>)</th>
								</tr>
							</thead>
							@foreach($x as $percentage)
								<tr class="info">
									<td> {{$percentage->name}}</td>
									<td>{{number_format($percentage->total*100/$totalIdea,2,'.','')}} %</td>

								</tr>
							@endforeach
						</table>
						</div>
					</div>

				<hr>

				<div class="col-md-6">
					<h4>Anonymous ideas and comments</h4>
					<div class="well">
						Total Anonymous ideas: {{$annonidea}}
						<a href="{{route('annomynousIdeaAndComment')}}" class="btn btn-info"> View all</a>
					</div>

					<div class="well">
						Total Anonymous comments: {{$annonComment}}
						<a href="{{route('annomynousIdeaAndComment')}}"  class="btn btn-info"> View all</a>
					</div>
				</div>

				<hr>
				<div class="col-md-6" >
					<h4>Number of contributors from each department</h4>
					<div class="table-responsive">
						<table class="table">
							<thead>
							<tr>
								<th>Department name</th>
								<th>Total contributor</th>
							</tr>
							</thead>
							<tbody>
							@foreach($contributor as $contr)
								<tr class="success">
									<td>{{$contr->dname}}</td>
									<td>{{$contr->contributor}}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>


		<hr>
		<div class="row">
			<div class="col-md-12">

			</div>
		</div>
	@endif


@endsection

@section('script')
<script>
    $(document).ready(function () {
        'use strict';
        $.ajax({
            url:"{{route('chart')}}",
            method:"GET",
            success:function(data){
                var dptname =[];
                var totalstudent= [];

                for(var i in data){
                    dptname.push("Dpt: "+data[i].dname);
                    totalstudent.push(data[i].totalstudent);
                }

                var BARCHARTHOME = $('#totalstudent');
                var barChartHome = new Chart(BARCHARTHOME, {
                    type: 'bar',
                    options:
                        {
                            scales:
                                {
                                    xAxes: [{
                                        display:true,
                                        gridLines: {
                                            color: '#eee'
                                        }
                                    }],
                                    yAxes: [{
                                        display: true,
                                        ticks: {
                                            beginAtZero:true
                                        }
                                    }],
                                },

                            legend: {
                                display: true
                            }
                        },
                    data: {
                        labels: dptname,
                        datasets: [
                            {
                                label: 'Total student',
                                backgroundColor:['#4286f4','#011b44','#7a020e','#1e0149',
                                    '#033302','#827705','#f93100','#033d33','#0560ff',
                                    '#ff054f','#4f0219','#ff477e','#130344','#024430',
                                    '#66ffd0','#214700','#ff0000','#ffae00','#29560d',
                                    '#02ccff','#014dff','#2c066d',
                                    '#280033','#c83bef','#260130','#4f002b','#190811'],

                                borderColor:
                                    '#fff',
                                borderWidth: 1,
                                data:totalstudent,
                            }
                        ]
                    }
                });
            },

            error:function (data) {
//                console.log(data);
            }

        });
    });



    $(document).ready(function () {
        'use strict';
        $.ajax({
            url:"{{route('total_post_by_each_dept')}}",
            method:"GET",
            success:function(data){
                console.log(data);
                var dptname =[];
                var totalIdea= [];

                for(var i in data){
                    dptname.push("Total idea of "+data[i].dname);
                    totalIdea.push(data[i].total_idea_of_each_dept);
                }
                var BARCHARTHOME1 = $('#totalIdeaofEachDept');
                var barChartHome1 = new Chart(BARCHARTHOME1, {
                    type: 'pie',

                    data: {
                        labels: dptname,
                        datasets: [
                            {
                                backgroundColor:['#4286f4','#011b44','#7a020e','#1e0149',
                                    '#033302','#827705','#f93100','#033d33','#0560ff',
                                    '#ff054f','#4f0219','#ff477e','#130344','#024430',
                                    '#66ffd0','#214700','#ff0000','#ffae00','#29560d',
                                    '#02ccff','#014dff','#2c066d',
                                    '#280033','#c83bef','#260130','#4f002b','#190811'],
                                borderColor:
                                    '#fff',
                                borderWidth: 1,
                                data:totalIdea,
                            }
                        ]
                    }
                });
            },

            error:function (data) {
                console.log(data);
            }

        });
    });






</script>





@endsection