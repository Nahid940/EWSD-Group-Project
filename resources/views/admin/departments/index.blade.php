@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">DEPARTMENT</span>
        <button type="button" class="btn btn-default pull-right hidden-print" data-toggle="modal" data-target="#createModal"><i class="glyphicon glyphicon-plus"> </i> Add New Department</button>
        <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        <button id="btnRefresh" class="btn btn-success pull-right hidden-print" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></button>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="70%">Department Name</th>
                    <th class="hidden-print">Action</th>
                </tr>
            </thead>
            <tbody id="departmentRows">
                
            </tbody>
        </table>
      </div>
    </div>

    <!--Create Modal-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form id="createAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Add New Department</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
               {{--  <input type="hidden" name="_token" value=""> --}}
                <label for="name">Department Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Department Name" required>
                </div>    
                <label for="head_name">Department Head Name</label>
                <div class="form-group">
                  <input type="text" name="head_name" id="head_name" class="form-control" placeholder="Enter Department Head Name">
                </div>          
                <br>                  
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btndeptsave" disabled> <i class="glyphicon glyphicon-ok" > </i>  Save</button>
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!--Edit Modal-->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form class="editAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Edit Department</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
               {{--  <input type="hidden" name="_token" value=""> --}}
                <label for="name">Department Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Department Name" required>
                </div>    
                <label for="head_name">Department Head Name</label>
                <div class="form-group">
                  <input type="text" name="head_name" id="head_name" class="form-control" placeholder="Enter Department Head Name">
                </div>          
                <br>                  
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btnUpdateDepartment"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>   
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <!--Show Modal-->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Department Details</h4>
          </div>
          <div class="modal-body">
            <p id="body"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--Message Modal-->
    <div class="modal fade" id="deleteConfirmationModel" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p id="body">Are You Sure to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>            
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
@endsection

@section('script')

    <script>
        $(document).ready(function() {
            $('#alertMessageContainer').css({'display':'none'});
            $('#btnRefresh').on('click', function() {
                loadDepartment();
            });

            loadDepartment();

            function loadDepartment() {
                $.ajax({
                    url: "{{ route('departments.index') }}",
                    type: 'GET',
                    success: function(departments) {
                        var currentDepartments = JSON.parse(departments);
                        var resultContainer = $('#departmentRows');
                        var rows = '', showUrl = '', token = '', number = 1;
                        currentDepartments.map(function(department) {
                            showUrl = ' {{ route('departments.show', ['department' => ':departmentId']) }}';
                            showUrl = showUrl.replace(':departmentId', department.id);
                            
                            rows += '<tr>' +
                                    '<td>' + number++ + '</td>' +
                                    '<td>' + department.name + '</td>' +
                                    '<td>' + 
                                    '<button type="button" class="btnShow hidden-print btn btn-success glyphicon glyphicon-eye-open" title="Show" value="'+ department.id +'"></button>' + ' '+
                                    '<button type="button" class="btnEdit hidden-print btn btn-warning glyphicon glyphicon-edit" title="Edit" value="'+ department.id +'"></button>' + ' ' +
                                    '<button type="button" class="btnDelete hidden-print btn btn-danger glyphicon glyphicon-trash" title="Delete" value="'+ department.id +'"></button>' 
                                    + '</td>' +
                                    '</tr>'
                        });
                        resultContainer.html(rows);
                        $('.btnShow').on('click', openShowModal);
                        $('.btnEdit').on('click', openEditModal);
                        $('.btnDelete').on('click', showDeleteConfirmation);
                    }
                });
            }

            //Department create form validate
            $(document).keyup('#createAjax input',function () {
                if(!isNaN($('#createAjax #name').val()) || $('#createAjax #name').val()=='')
                {
                    $('#createAjax #name').css("border-color", "red");
                    $("#btndeptsave").attr("disabled", "true");
                }
                else if(!isNaN($('#createAjax #head_name').val()) || $('#createAjax #head_name').val()=='')
                {
                    $('#createAjax #head_name').css("border-color", "red");
                    $("#btndeptsave").attr("disabled", "true");
                }else{
                    $('#createAjax #name').css("border-color", "green");
                    $('#createAjax #head_name').css("border-color", "green");
                    $("#btndeptsave").removeAttr('disabled');
                }
            });

            $('#createAjax').on('submit', function(e)  {
                e.preventDefault();

                $.ajax({
                    url: "{{ route('departments.store') }}",
                    type: 'POST',
                    data: $("#createAjax").serialize(),
                    //console.log(replyObject);
                    success: function (result) {
                        var replyObject = JSON.parse(result);
                        //console.log(replyObject.alertType);
                        //$('#alertMessageContainer').css({'display':'block'});

                        $('#alertMessageContainer').addClass(replyObject.alertType);
                        $('#alertMessage').html(replyObject.alertMessage);
                        $('alertMessageContainer').show();
                        $('#createAjax')[0].reset();
                        loadDepartment();
                    }
                });
            });

            function openShowModal() {
                var showableDepartmentId = $(this).val();
                showUrl = ' {{ route('departments.show', ['department' => ':departmentId']) }}';
                showUrl = showUrl.replace(':departmentId', showableDepartmentId);
                $.ajax({
                    url: showUrl,
                    type: 'GET',
                    success: function(result) {
                        var replyObject = JSON.parse(result);
                        $('#body').html([
                            'Department Name: ' + replyObject.name + '<br>',
                            'Head Name: ' + replyObject.head_name + '<br>',
                        ]);
                    }
                });
                $('#showModal').modal();
            }

            function openEditModal() {
                var editableDepartmentId = $(this).val();
                var showUrl = '{{ route('departments.edit', ['department' => ':departmentId']) }}';
                showUrl = showUrl.replace(':departmentId', editableDepartmentId);

                $.ajax({
                    url: showUrl,
                    type: 'GET',
                    success: function(result) {
                        var replyObject = JSON.parse(result);
                        $('.editAjax #name').val(replyObject.name);
                        $('.editAjax #head_name').val(replyObject.head_name);

                        $('#btnUpdateDepartment').val(editableDepartmentId);
                        $('#editModal').modal();
                    }
                });
            }

            //Department update form validate
            $(document).keyup('.editAjax input',function () {
                if(!isNaN($('.editAjax #name').val()))
                {
                    $('.editAjax #name').css("border-color", "red");
                    $("#btnUpdateDepartment").attr("disabled", "true");
                }
                else if(!isNaN($('.editAjax #head_name').val()))
                {
                    $('.editAjax #head_name').css("border-color", "red");
                    $("#btnUpdateDepartment").attr("disabled", "true");
                }else{
                    $('.editAjax #name').css("border-color", "green");
                    $('.editAjax #head_name').css("border-color", "green");
                    $("#btnUpdateDepartment").removeAttr('disabled');
                }
            });

            function updateDepartment() {
                var updateableDepartmentId = $(this).val();
                var updateUrl = '{{ route('departments.update', ['department' => ':departmentId']) }}';
                updateUrl = updateUrl.replace(':departmentId', updateableDepartmentId);
               // var theTokenVal = '{{ csrf_token() }}';
                var formData = $("form.editAjax").serialize() + "&_method=PATCH";
                //console.log(formData);
                $.ajax({
                    url: updateUrl,
                    type: 'POST',
                    data: formData,
                    success: function(result) {
                        console.log(result);  
                        loadDepartment();                    
                    }
                });
            }

            $('#btnUpdateDepartment').on('click', updateDepartment);

            function showDeleteConfirmation() {
                var deleteDepartmentId = $(this).val();
                $('#deleteButton').val(deleteDepartmentId);
                $('#deleteConfirmationModel').modal();
            }

            $('#deleteButton').on('click', deleteDepartment);

            function deleteDepartment() {
                var deleteableDepartmentId = $(this).val();
                var deleteUrl = '{{ route('departments.destroy', ['department' => ':departmentId']) }}';
                deleteUrl = deleteUrl.replace(':departmentId', deleteableDepartmentId);

                $.ajax({
                    url : deleteUrl,
                    type : 'POST',
                    data : {
                        "_token": '{{ csrf_token() }}',
                        "_method": 'DELETE'
                    },
                    success: function(result) {
                        loadDepartment();
                        $('#deleteConfirmationModel').modal('hide');
                    }
                });
            }
        });
    </script>

@endsection