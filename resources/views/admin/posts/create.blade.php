@extends('layouts.admin')


@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">Add New IDEA</span>
        
        <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
    
        <form method="POST" action="{{ route('posts.store') }}">      
          
            {{ csrf_field() }}
      
              <input type="hidden" name="user_id" id="user_id" class="form-control" value="{{ Auth::user()->id }}"> 
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tag_id">Tag Name</label>
                  <select class="form-control tag_id" name="tag_id" id="tag_id">
                    
                    @foreach ($tags as $tag)
                       <option value="{{ $tag->tag_id }}">{{ $tag->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="col-md-6">
                <label for="status">Status</label>
                <div class="form-group">
                  <select class="form-control" name="status">
                    <option value="0">Active</option>
                    <option value="1">Inactive</option>
                  </select>
                </div>
              </div>
              
              <div class="col-md-12">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" name="title" id="title" class="form-control">
                </div>
              </div>
              <div class="col-md-12">
                <label for="body">Body</label>
                <div class="form-group">
                  <textarea type="text" cols="3" rows="3" class="form-control" name="body"></textarea>
                </div>
              </div>
                 
              
              <div class="col-md-12">
                <label>&nbsp;</label><br>
                <a type="button" style="margin-left: 20px;" href="{{ route('posts.index') }}" class="btn btn-danger pull-right"> <i class="glyphicon glyphicon-remove"> </i> Cancel</a>
                <button type="submit" class="btn btn-primary m-t-15 waves-effect pull-right"> <i class="glyphicon glyphicon-ok"> </i>  Save</button>
                
              </div>
    
        
        </form>
    
        
      </div>
    </div>


   
  
@endsection

