@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
        <div class="panel-heading">
            <span class="heading">Ideas with no comments | </span>
            <span class="heading"> Total idea {{sizeof($nocommentIdeas)}}</span>
            <button style="margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Posted by</th>
                    <th>Idea</th>
                </tr>
                </thead>
                <tbody>

                @for($i=0;$i<sizeof($nocommentIdeas);$i++)
                    <tr>
                        <td>1</td>
                        <td>
                            {{$nocommentIdeas[$i]['user_name']}}
                        </td>
                        <td>
                          {{$nocommentIdeas[$i]['body']}}
                        </td>
                        <td class="hidden-print">
                            <a  href="{{route('individualIdeaWithNoComment',['id'=>$nocommentIdeas[$i]['id']])}}"  class="btn btn-success" id="ideabutton">View details</a>
                        </td>
                    </tr>

                @endfor
                {{--@endforeach--}}
                </tbody>
            </table>
        </div>
    </div>
@endsection