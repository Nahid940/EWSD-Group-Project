@extends('layouts.admin')

@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading"><h4>Control Idea post</h4></div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Session name</th>
                            <th>Validity</th>
                            <th>Idea post</th>
                        </tr>
                        @foreach($sessions as $s)
                            <tr>
                                <td>{{$s->name}}</td>
                                <td>
                                    @if($s->end_date > date('Y-m-d') )
                                        <h4><span for="" class="label label-success">Session On going</span></h4>
                                    @elseif($s->end_date < date('Y-m-d'))
                                        <span for="" class="label label-danger">Session over</span>
                                    @endif
                                </td>


                                <td>
                                @if($s->contorlIdeaPost!=null)
                                    @if($s->contorlIdeaPost->closure_date >= date('Y-m-d'))
                                        <h4><span for="" class="label label-success">Student can post idea</span></h4>
                                            <button class="btn btn-danger"  data-toggle="modal" data-target="#existingDate" id="DisableIdea_post" data-id="{{$s->id}}">Disable idea post</button>
                                    @else
                                        <span for="" class="label label-danger">Closed</span>
                                        <button class="btn btn-warning"  data-toggle="modal" data-target="#existingDate" id="DisableIdea_post" data-id="{{$s->id}}">Renew idea post</button>
                                    @endif
                                @else
                                    <button class="btn btn-info"  data-toggle="modal" data-target="#myModal" id="enableIdea_post" data-id="{{$s->id}}">Enable idea post</button>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="sessionid">
                    <dov class="row">
                        <div class="form-group">
                            <label for="">Closure date</label>
                            <input type="date" name="closure_date" id="closure_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Final close</label>
                            <input type="date" name="final_close" id="final_close" class="form-control">

                        </div>
                    </dov>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="ideacontrolsubmit">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div id="existingDate" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Disable Idea sharing</h4>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="enable_sessionid">
                    <dov class="row">
                        <div class="form-group">
                            <label for="">Closure date</label>
                            <input type="date" name="closure_date" id="enableclosure_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Final close</label>
                            <input type="date" name="final_close" id="enablefinal_close" class="form-control">

                        </div>
                    </dov>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="disbaleIdea">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

        $(document).on('click','#enableIdea_post',function () {
            $('#sessionid').val($('#enableIdea_post').data('id'));
        })

        $(document).on('click','#ideacontrolsubmit',function () {
            $.ajax({
                type:'post',
                url:'{{route('newPErmitForIdea')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    closure_date:$('#closure_date').val(),
                    final_close:$('#final_close').val(),
                    id:$('#sessionid').val()
                },
                success:function (data) {
                    window.location.reload();
                }
            })
        })

        $(document).on('click','#DisableIdea_post',function () {
            $.ajax({
                type:'post',
                url:'{{route('existingDate')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    id:$(this).data('id')
                },

                success:function (data) {
                    $('#enableclosure_date').val(data.closure_date);
                    $('#enablefinal_close').val(data.final_close);
                    $('#enable_sessionid').val(data.session_id);
                },
                error:function (d) {
                    alert(d.responseText);
                }
            })
        })


        $(document).on('click','#disbaleIdea',function () {
            $.ajax({
                type:'post',
                url:'{{route('disableIdeaControl')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    closure_date:$('#enableclosure_date').val(),
                    final_close:$('#enablefinal_close').val(),
                    id:$('#enable_sessionid').val()
                },
                success:function (data) {
//                    console.log(data);

                    window.location.reload();
                }
            })
        })
    </script>
@endsection