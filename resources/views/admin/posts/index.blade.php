@extends('layouts.admin')

@section('css')
  <link href="https://fonts.googleapis.com/css?family=Arsenal" rel="stylesheet">
<style type="text/css">

  input.rounded {
  margin-right: 10px;
  border: 1px solid #ccc;
  -moz-border-radius: 20px;
  -webkit-border-radius: 20px;
  border-radius: 5px;
  -moz-box-shadow: 2px 2px 3px #666;
  -webkit-box-shadow: 2px 2px 3px #666;
  box-shadow: 2px 2px 3px #666;
  font-size: 18px;
  padding: 4px 7px;
  outline: 0;
  -webkit-appearance: none;
}

  .glyphicon.glyphicon-comment{
    font-size: 25px;
  }
    a:link {
    text-decoration: none;
}
  a:hover {
    text-decoration: none;
}
</style>
@endsection

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">IDEA</span>
{{--         <a type="button" href="{{route('posts.create')}}" class="btn btn-default pull-right hidden-print"> <i class="glyphicon glyphicon-plus"> </i> Add New Idea</a>--}}
        <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>

        <div class="clearfix"></div>

      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home">All ideas</a></li>
          <li><a data-toggle="tab" href="#menu1">Most popular ideas</a></li>
          <li><a data-toggle="tab" href="#menu2">Most viewed ideas</a></li>
        </ul>

        <div class="tab-content">
          <div id="home" class="tab-pane fade in active">

            @foreach ($posts as $post)
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
              <span class="pull-right">
                 <form action="{!! action('PostController@destroy', $post->id) !!}" method="POST" style="display: inline-block;margin-left: 5px;">
                              {{ csrf_field() }} {{ method_field('DELETE') }}
                   <button type="submit" title="Delete" role="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash" title="Delete"></i></button>
                  </form>
                </span>
                    

                    <h4 class="modal-title"><a href="">Idea title {{ $post->title }}</a></h4>
                    {{--              @php $d=\App\Department::select('name')->where('id', $post->user->student['department_id'])->first() @endphp--}}

                    <p class="lead">
                      Posted by {{ $post->user->name }} from Department of <span style="color :#9b0017">{{$post->user->student->department->name}}</span>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $post->created_at->diffForHumans() }}
                    </p>
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#comments_{{ $post->id }}" > {{sizeof($post->comment)}} comments</a>
                  </div>
                  <div class="modal-body">
                    <p>{{ $post->body }}</p>

                  </div>
                  <div class="modal-footer">

                    <span id="message{{$post->id}}" class="messagespan" style="color: #ff2626;font-weight: bold"></span>

                    @php
                      $d= \App\PostLike::where('post_id',$post->id)->where('user_id',\Illuminate\Support\Facades\Auth::user()->id)->first();
                    @endphp

                    @if($d!=null)
                      @if($d->likes=='1'  && $d->user_id==\Illuminate\Support\Facades\Auth::user()->id)
                        <span id="likebutton{{$post->id}}">
                                            <a href="javascript:void(0)" class="thumbs_up" data-id="{{$post->id}}"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                        </span>

                        <span id="dislikebutton{{$post->id}}">
                                            <a href="javascript:void(0)" class="thumbs_down" data-id="{{$post->id}}"><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                        </span>

                      @elseif($d->dislike=='-1' && $d->user_id==\Illuminate\Support\Facades\Auth::user()->id)
                        <span id="likebutton{{$post->id}}">
                                              <a href="javascript:void(0)" class="thumbs_up" data-id="{{$post->id}}"><span class="glyphicon glyphicon-thumbs-up"></span></a>
                                          </span>

                        <span id="dislikebutton{{$post->id}}">
                                              <a href="javascript:void(0)" class="thumbs_down" data-id="{{$post->id}}"><i class="fa fa-thumbs-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                          </span>
                      @endif
                    @else


                      <span id="likebutton{{$post->id}}">
                                      <a href="javascript:void(0)" class="thumbs_up" data-id="{{$post->id}}"><i class="fa fa-thumbs-o-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                  </span>
                      <span id="dislikebutton{{$post->id}}">
                                       <a href="javascript:void(0)" class="thumbs_down" data-id="{{$post->id}}"><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>
                                  </span>

                    @endif

                    <a type="button" class="glyphicon glyphicon-comment" title="Leave a Comments" data-toggle="collapse" data-target="#comments_{{ $post->id }}" aria-expanded="false" aria-controls="comments_{{ $post->id }}"></a>


                    <div class="collapse" id="comments_{{ $post->id }}">
                      <div class="well well-sm pull-left">
                        <div class="row">
                          <div class="col-md-12">
                            <form class="form-inline" method="post" action="{{ url('comment') }}">
                              {{ csrf_field() }}
                              <input type="hidden" name="post_id" value="{{ $post->id }}">
                              <input type="hidden" name="user_id" value="{{ Auth::user()->id}}">
                              <div class="form-group">
                                <input  required size="60" required name="comment" class="form-control rounded" type="text" placeholder=" Write a comment......">
                              </div>
                              <div style="margin-right: 10px;" class="checkbox">
                                <label>
                                </label>
                              </div>
                              <button type="submit" class="btn btn-primary ">Comment</button>
                            </form><hr>
                            <!-- Comment -->
                            @foreach ($post->comment as $comment)
                              <div class="media">
                                <a class="pull-left" href="#">
                                  <img class="media-object" src="{{asset('uploads/users/'.$post->user->image)}}" alt="" height="50px" width="50px">
                                </a>
                                <div class="media-body">
                                  <p style="text-align: left;">
                                    {{$comment->user->name}}
                                    <small>{{$comment->created_at->diffForHumans()}}</small><br>
                                    {{ $comment->comment}}

                                  </p>

                                </div>
                              </div>
                            @endforeach
                          </div>
                        </div>
                        <hr>
                      </div>

                      <div class="clearfix"></div>
                    </div>
                  </div>

                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            @endforeach
          </div>





          <div id="menu1" class="tab-pane fade">
            <h3>Most popular ideas</h3>

            @php $i=0; @endphp
            @foreach($likes as $l)
              @php $i++; @endphp
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">

              <span class="pull-right">
                <p style="color: #630027;font-size: 15px"><i class="fa fa-certificate" aria-hidden="true" style="color: #0aaf01"></i> <b>Popular idea {{$i}} | Total likes {{$l->total_like}}</b></p>
                </span>
                    <h4 class="modal-title"><a href="">Idea title {{ $l->post->title }}</a></h4>
                    <p class="lead">
                      Posted by {{ $l->post->user->name }} from Department of <span style="color :#9b0017">{{$l->post->user->student->department->name}}</span>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $l->post->created_at->diffForHumans() }}
                    </p>
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#comments_{{ $i}}" > {{sizeof($l->post->comment)}} comments</a>
                  </div>
                  <div class="modal-body">
                    <p>{{ $l->post->body }}</p>
                  </div>
                  <div class="modal-footer">

                    <a type="button" class="glyphicon glyphicon-comment" title="Leave a Comments" data-toggle="collapse" data-target="#comments_{{ $i }}" aria-expanded="false" aria-controls="comments_{{ $post->id }}"></a>

                    <div class="collapse" id="comments_{{ $i }}">
                      <div class="well well-sm pull-left">
                        <div class="row">
                          <div class="col-md-12">
                            <form class="form-inline" method="post" action="{{ url('comment') }}">
                              {{ csrf_field() }}
                              <input type="hidden" name="post_id" value="{{ $l->post->id }}">
                              <input type="hidden" name="user_id" value="{{ Auth::user()->id}}">
                              <div class="form-group">
                                <input size="60" name="comment" required class="form-control rounded" type="text" placeholder=" Write a comment......">
                              </div>
                              <div style="margin-right: 10px;" class="checkbox">
                                <label>
                                </label>
                              </div>
                              <button type="submit" class="btn btn-primary ">Comment</button>
                            </form><hr>
                            <!-- Comment -->
                            @foreach ($l->post->comment as $comment)
                              <div class="media">
                                <a class="pull-left" href="#">
                                  <img class="media-object" src="{{asset('uploads/users/'.$l->user->image)}}" alt="" height="50px" width="50px">
                                </a>
                                <div class="media-body">
                                  <p style="text-align: left;">
                                    {{$comment->user->name}}
                                    <small>{{$comment->created_at->diffForHumans()}}</small><br>
                                    {{ $comment->comment}}
                                  </p>

                                </div>
                              </div>
                            @endforeach
                          </div>
                        </div>
                        <hr>
                      </div>

                      <div class="clearfix"></div>
                    </div>
                  </div>

                </div><!-- /.modal-content -->
              </div>


            @endforeach
          </div>
          <div id="menu2" class="tab-pane fade">
            <h3>Most viewed ideas</h3>
            @php $i=0; @endphp
            @foreach($views as $l)
              @php $i++ @endphp
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">

              <span class="pull-right">
                <p style="color: #630027;font-size: 15px"><i class="fa fa-certificate" aria-hidden="true" style="color: #0aaf01"></i> <b>Most viewed idea {{$i}} | Total views {{$l->total_view}}</b></p>
                </span>
                    <h4 class="modal-title"><a href="">Idea title {{ $l->post->title }}</a></h4>
                    <p class="lead">
                      Posted by {{ $l->post->user->name }} from Department of <span style="color :#9b0017">{{$l->post->user->student->department->name}}</span>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on {{ $l->post->created_at->diffForHumans() }}
                    </p>
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#ideacomments_{{ $i}}" > {{sizeof($l->post->comment)}} comments {{$i}}</a>
                  </div>
                  <div class="modal-body">
                    <p>{{ $l->post->body }}</p>
                  </div>
                  <div class="modal-footer">

                    <a type="button" class="glyphicon glyphicon-comment" title="Leave a Comments" data-toggle="collapse" data-target="#ideacomments_{{ $i }}" aria-expanded="false" aria-controls="comments_{{ $i }}"></a>

                    <div class="collapse" id="ideacomments_{{ $i }}">
                      <div class="well well-sm pull-left">
                        <div class="row">
                          <div class="col-md-12">
                            <form class="form-inline" method="post" action="{{ url('comment') }}">
                              {{ csrf_field() }}
                              <input type="hidden" name="post_id" value="{{ $l->post->id }}">
                              <input type="hidden" name="user_id" value="{{ Auth::user()->id}}">
                              <div class="form-group">
                                <input size="60" name="comment" required class="form-control rounded" type="text" placeholder=" Write a comment......">
                              </div>
                              <div style="margin-right: 10px;" class="checkbox">
                                <label>
                                </label>
                              </div>
                              <button type="submit" class="btn btn-primary ">Comment</button>
                            </form><hr>
                            <!-- Comment -->
                            @foreach ($l->post->comment as $comment)
                              <div class="media">
                                <a class="pull-left" href="#">
                                  <img class="media-object" src="{{asset('uploads/users/'.$l->user->image)}}" alt="" height="50px" width="50px">
                                </a>
                                <div class="media-body">
                                  <p style="text-align: left;">
                                    {{$comment->user->name}}
                                    <small>{{$comment->created_at->diffForHumans()}}</small><br>
                                    {{ $comment->comment}}
                                  </p>

                                </div>
                              </div>
                            @endforeach
                          </div>
                        </div>
                        <hr>
                      </div>

                      <div class="clearfix"></div>
                    </div>
                  </div>

                </div><!-- /.modal-content -->
              </div>


            @endforeach
          </div>
        </div>




        {{$posts->links()}}
      </div>
    </div>


   
  
@endsection
@section('script')

  <script>
      $(document).on('click','.thumbs_up',function () {
          var postid=($(this).data('id'));


          $.ajax({
              type:'post',
              url:'{{route('postLike')}}',
              data:{
                  _token:'{{csrf_token()}}',
                  post_id:$(this).data('id'),
                  user_id:'{{\Illuminate\Support\Facades\Auth::user()->id}}',
                  like:'1',
                  dislike:'0',
                  Buttonaction:'like'
              },
              success:function (data) {
                  if(data=='done')
                  {
                      $('#message'+postid).fadeIn();
                      $('#message'+postid).text('You have already liked this post !!');
                      setTimeout(function() {
                          $('#message'+postid).fadeOut('slow');
                      }, 2000);

                  }else if(data['newreocrdadded']){
                      $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"  ><i class="fa fa-thumbs-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                      $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'"><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                  }else{
                      $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"><i class="fa fa-thumbs-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                      $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'" ><i class="fa fa-thumbs-o-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                  }
              },
              error:function (err) {
                  alert(err.responseText)
              }
          })
      })
      $(document).on('click','.thumbs_down',function () {
          var postid=($(this).data('id'));
          $.ajax({
              type:'post',
              url:'{{route('postLike')}}',
              data:{
                  _token:'{{csrf_token()}}',
                  post_id:$(this).data('id'),
                  user_id:'{{\Illuminate\Support\Facades\Auth::user()->id}}',
                  like:'0',
                  dislike:'-1',
                  Buttonaction:'dislike'
              },
              success:function (data) {
                  if(data=='done')
                  {
                      $('#message'+postid).show();
                      $('#message'+postid).text('You have already disliked this post !!');
                      setTimeout(function() {
                          $('#message'+postid).hide('slow');
                      }, 3000);

                  }else if(data['newreocrdadded']=='ok'){

                      $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"  ><i class="fa fa-thumbs-o-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                      $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'" ><i class="fa fa-thumbs-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                  }else{
                      $('#likebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_up" data-id="'+data['id']+'"><i class="fa fa-thumbs-o-up" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                      $('#dislikebutton'+postid).html('<a href="javascript:void(0)" class="thumbs_down" data-id="'+data['id']+'"><i class="fa fa-thumbs-down" aria-hidden="true" style="color: #38cdff;font-size: 25px"></i></a>');
                  }
              },
              error:function (err) {
                  alert(err.responseText)
              }
          })
      })
  </script>

@endsection

