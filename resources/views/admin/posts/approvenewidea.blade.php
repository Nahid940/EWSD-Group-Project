@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
        <div class="panel-heading">
            <span class="heading">IDEA</span>
            <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>

            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Idea Title</th>
                    <th>Idea</th>
                    <th>Document</th>
                    <th>Posted by</th>
                    <th class="hidden-print">Action</th>
                </tr>
                </thead>
                <tbody id="postRows">

                </tbody>
            </table>

        </div>
    </div>


    <!--Create Modal-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form id="createAjax" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="heading">Add New Idea</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_id" id="user_id" class="form-control" value="{{ Auth::user()->id }}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tag_id">Tag Name</label>
                                    <select class="form-control tag_id" name="tag_id" id="tag_id">
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="post_date">Date</label>
                                <div class="form-group">
                                    <input type="date" name="post_date" id="post_date" class="form-control"  required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" id="title" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="body">Body</label>
                                <div class="form-group">
                                    <textarea type="text" cols="3" rows="3" class="form-control" name="body"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Privacy</label>
                                <div class="checkbox">
                                    <label><input type="checkbox" value="0" name="profile_privacy"> Yes</label>
                                    <label><input type="checkbox" value="1" name="profile_privacy"> No</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="status">Status</label>
                                <div class="form-group">
                                    <select class="form-control" name="status">
                                        <option value="0">Active</option>
                                        <option value="1">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"> <i class="glyphicon glyphicon-ok"> </i>  Save</button>

                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                    </div>
                </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->




    <!--Show Modal-->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="heading">Post Details</h4>
                </div>
                <div class="modal-body">
                    <p id="body"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->




    <!--Edit Modal-->
    <div class="modal fade" id="userInfomodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form class="editAjax">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="heading">User info</h4>
                    </div>
                    <div class="modal-body">


                        <div class="row">

                            <table class="table">
                                <tr>
                                    <td>
                                        <div id="image">

                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><label for="tag_id" id="user_name"></label></td>
                                    <td>  <label for="post_date" id="dept_name"></label></td>
                                </tr>
                                <tr>
                                    <td><label for="" id="session"></label></td>
                                    <td><label for="body" id="batch"></label></td>
                                </tr>
                            </table>
                        </div>


                    </div>
                </div><!-- /.modal-content -->
            </form>
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!--Message Modal-->
    <div class="modal fade" id="approveConfirmationModel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="approveideaID">
                    <p id="body">Are you sure to approve this idea?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="approveButton">Approve</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->




    <!--Message Modal-->
    <div class="modal fade" id="deleteConfirmationModel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idea_delete_id">
                    <p id="body">Are You Sure to delete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('script')

    <script>
        $(document).ready(function() {
            $('#alertMessageContainer').css({'display':'none'});
            $('#btnRefresh').on('click', function() {
                loadPost();
            });

            loadPost();

            function loadPost() {
                $.ajax({
                    url: "{{ route('newidea') }}",
                    type: 'GET',
                    success: function(posts) {
//                        console.log(posts[2].document);
                        var resultContainer = $('#postRows');
                       var rows = '', showUrl = '', token = '', number = 1;

                       if(posts.length>=1) {
                           posts.map(function (post) {
                               showUrl = ' {{ route('posts.show', ['post' => ':postId']) }}';
                               showUrl = showUrl.replace(':postId', post.id);

                               rows += '<tr>' +
                                   '<td>' + number++ + '</td>' +
                                   '<td>' + post.title + '</td>' +
                                   '<td>' + post.body + '</td>';
//                                '<td>' + post.document + '</td>' +
                               if (post.document != null) {
                                   rows += '<td>' + post.document.doc + ' <a href="uploads/files/' + post.document.doc + '" class="btn btn-info">View</a></td>';
                               } else {
                                   rows += '<td><span style="color: red">No document shared</span></td>';
                               }
                               rows += '<td>' + post.user.name + '</td>' +
                                   '<td>' +
                                   '<button type="button" id="approve" class="btnShow hidden-print btn btn-success glyphicon glyphicon-ok" title="approve" data-id="' + post.id + '"></button>' + ' ' +
                                   '<button type="button" class="btnDelete hidden-print btn btn-danger glyphicon glyphicon-remove" title="Discard" value="' + post.id + '"></button>' + " " +
                                   '<button type="button" class="btnuserInfo hidden-print btn btn-info glyphicon glyphicon-user" title="Get user details" value="' + post.user.id + '"></button>'
                                   + '</td>' +
                                   '</tr>';


                           });
                       }else{
                           rows+='<h4 style="color: red;">Empty !</h4>';
                       }
                        resultContainer.html(rows);
                        //$('.btnShow').on('click', openShowModal);
//                        $('.btnEdit').on('click', openEditModal);
//                        $('.btnDelete').on('click', showDeleteConfirmation);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                            alert('Internal error: ' + jqXHR.responseText);

                    }
                });
            }


            $('.btnModalCreate').on('click', function() {
                $.ajax({
                    url: "{{ route('posts.create') }}",
                    type: 'GET',
                    success: function (results) {
                        //console.log(results);
                        var currentDepartments = results;
                        var resultContainer = $('#tag_id');
                        var option = "";
                        results.map(function(result) {
                            option += "<option value='" + result.id +"'>"+ result.name +"</option>"
                        });
                        resultContainer.html(option);
                    }
                });
                $('#createModal').modal();
            });


            $('#createAjax').on('submit', function(e)  {
                e.preventDefault();

                $.ajax({
                    url: "{{ route('posts.store') }}",
                    type: 'POST',
                    data: $("#createAjax").serialize(),
                    //console.log(replyObject);
                    success: function (result) {
                        // console.log(result);
                        var replyObject = JSON.parse(result);
                        //console.log(replyObject);
                        $('#alertMessageContainer').css({'display':'block'});

                        $('#alertMessageContainer').addClass(replyObject.alertType);
                        $('#alertMessage').html(replyObject.alertMessage);
                        $('alertMessageContainer').show();
                        $('#createAjax')[0].reset();
                        loadPost();
                    }
                });
            });


            $(document).on('click','#approve',function () {
                var id=($(this).data('id'));
                $('#approveideaID').val(id);
                $('#approveConfirmationModel').modal();
            });


            function approvePsot() {
                var postid = $('#approveideaID').val();
//                alert(postid);
                $.ajax({
                    url : "{{route('approve')}}",
                    type : 'POST',
                    data : {
                        "_token": '{{ csrf_token() }}',
                        id:postid
                    },
                    success: function(result) {
                        window.location.reload();
                        $('#approveConfirmationModel').modal('hide');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Internal error: ' + jqXHR.responseText);
                    }
                });
            }
            $('#approveButton').on('click',function () {
                approvePsot();
            })


            $(document).on('click','.btnuserInfo',function () {
                var userID = $(this).val();
                $.ajax({

                    type:'post',
                    url:"{{route('getuserinfo')}}",
                    data:{
                        _token:"{{csrf_token()}}",
                        id:userID
                    },
                    success:function (data) {
                        console.log(data.department.name);
                        $('#image').html('<img width="100" height="100" src="/uploads/users/'+data.user.image+'">');
                        $('#user_name').text("Name : "+ data.user.name);
                        $('#dept_name').text("Department : "+data.department.name);
                        $('#session').text("Session : "+data.session.name);
                        $('#batch').text("Batch : "+data.batch.name);

                        $('#userInfomodal').modal();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Internal error: ' + jqXHR.responseText);
                    }
                });

            })

            $(document).on('click','.btnDelete',function (e) {
                e.preventDefault();
                var deleteablePostId = $(this).val();
                $('#deleteConfirmationModel').modal();
                $('#idea_delete_id').val(deleteablePostId);
            })
            


            $('#deleteButton').on('click',function () {
                deletePost();
            });

            function deletePost() {
                $.ajax({
                    url : '{{route('deletePost')}}',
                    type : 'POST',
                    data : {
                        "_token": '{{ csrf_token() }}',
                        id:$('#idea_delete_id').val()
                    },
                    success: function(result) {
                        loadPost();
                        console.log(result);
                        $('#deleteConfirmationModel').modal('hide');
                    },
                });
            }

        });
    </script>

@endsection