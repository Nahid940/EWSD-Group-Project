@extends('layouts.admin')
@section('content')

    <div class="row">


            <div class="col-md-6">

            @if(session('no_file'))
                <div class="alert alert-danger">
                    {{session('no_file')}}
                </div>
            @endif

                <div class="panel panel-success">
                    <div class="panel-heading">Download all file in zip !!</div>
                    <div class="panel-body">
                        @if($empty=='')
                            <h4>No fie to zip right now</h4>
                        @else
                        <form action="{{route('getallFile')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <select name="session_id" id="" class="form-control">
                                    @for($i=0;$i<sizeof($session_data);$i++)
                                        <option value="{{$session_data[$i]['id']}}">{{$session_data[$i]['name']}}</option>
                                    @endfor
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-briefcase"></span> Download zip</button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>

        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">All files</div>
                <div class="panel-body">


                    @if($empty=='')
                        <h4>No file to download right now !!</h4>
                        @else
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Idea</th>
                                <th>Document</th>
                                <th>Download</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($File as $f)
                                <tr>
                                    <td>{{$f->post->body}}</td>
                                    <td>{{$f->doc}}</td>
                                    <td><a href="{{asset('uploads/files/'.$f->doc)}}"> <span class="glyphicon glyphicon-download-alt"></span></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        @endif

                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    {{--<script src="{{asset('js/jszip.min.js')}}"></script>--}}
    {{--<script>--}}

        {{--$(document).ready(function () {--}}
            {{--var zip = new JSZip();--}}
            {{--zip.file("Hello.txt", "Hello World\n");--}}
            {{--var img = zip.folder("image");--}}
            {{--img.file("smile.gif", imgData, {base64: true});--}}
            {{--zip.generateAsync({type:"blob"})--}}
                {{--.then(function(content) {--}}
                    {{--// see FileSaver.js--}}
                    {{--saveAs(content, "example.zip");--}}
                {{--});--}}
        {{--})--}}

    {{--</script>--}}
@endsection