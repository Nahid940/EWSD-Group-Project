@extends('layouts.admin')

@section('css')
<style type="text/css">

  input.rounded {
  margin-right: 10px;
  border: 1px solid #ccc;
  -moz-border-radius: 20px;
  -webkit-border-radius: 20px;
  border-radius: 20px;
  -moz-box-shadow: 2px 2px 3px #666;
  -webkit-box-shadow: 2px 2px 3px #666;
  box-shadow: 2px 2px 3px #666;
  font-size: 18px;
  padding: 4px 7px;
  outline: 0;
  -webkit-appearance: none;
}

  .glyphicon.glyphicon-comment{
    font-size: 25px;
  }
    a:link {
    text-decoration: none;
}
  a:hover {
    text-decoration: none;
}
</style>
@endsection

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">Edit IDEA</span>
        
        <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
    
        <form method="POST" action="{{ route('posts.update', ['post' => $post->id]) }}">      
          
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
              <input type="hidden" name="user_id" id="user_id" class="form-control" value="{{ Auth::user()->id }}"> 
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tag_id">Tag Name</label>
                  <select class="form-control tag_id" name="tag_id" id="tag_id">
                    <option value="{{ $post->tag_id }}">{{ $post->tag->name }}</option>
                    @foreach ($tags as $tag)
                       <option value="{{ $tag->tag_id }}">{{ $tag->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <label for="post_date">Date</label>
                <div class="form-group">
                  <input type="date" name="created_at" id="post_date" class="form-control" value="{{ $post->created_at->format('Y-m-d') }}"  required>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" name="title" id="title" class="form-control" value="{{ $post->title }}">
                </div>
              </div>
              <div class="col-md-12">
                <label for="body">Body</label>
                <div class="form-group">
                  <textarea type="text" cols="3" rows="3" class="form-control" name="body">{{ $post->body }}</textarea>
                </div>
              </div>
                 
              <div class="col-md-6">
                <label for="status">Status</label>
                <div class="form-group">
                  <select class="form-control" name="status">
                    <option value="0">Active</option>
                    <option value="1">Inactive</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <label>&nbsp;</label><br>
                <a type="button" style="margin-left: 20px;" href="{{ route('posts.index') }}" class="btn btn-danger pull-right"> <i class="glyphicon glyphicon-remove"> </i> Cancel</a>
                <button type="submit" class="btn btn-primary m-t-15 waves-effect pull-right"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>
                
              </div>
    
        
        </form>
    
        
      </div>
    </div>


   
  
@endsection

