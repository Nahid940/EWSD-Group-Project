@extends('layouts.admin')
@section('css')
    {{--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">--}}
@endsection
@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Panel Heading</div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <label for="">Idea Categories</label>
                    <select name="tag" id="tag" class="form-control">
                        <option value="">Select tag</option>
                        @foreach($tags as $tg)
                            <option value="{{$tg->id}}">{{$tg->name}}</option>
                        @endforeach
                    </select>

                </div>
            </div>

            &nbsp;

            <div class="row">
                <div class="col-md-8 col-md-offset-2" id="tagWiseIdea">

                </div>
            </div>


        </div>
    </div>


@endsection

@section('script')
    <script>
        $(document).on('change','#tag',function () {
//            alert($('#tag').val());

            $.ajax({
                url:'{{route('getallideaByTag')}}',
                type:'post',
                data:{
                    _token:'{{csrf_token()}}',
                    id:$('#tag').val()
                },
                success:function (data) {
                    console.log(data);
                    var row='';
                    data.map(function (idea) {
                        row+= '<div class="panel panel-info">' +
                            '<div class="panel-heading">Posted by '+idea.user.name+' | Tag <b>'+idea.tag.name+'</b></div>' +
                            '<div class="panel-body">' +
                            '<p>Idea title: '+idea.title+'</p>' +
                            '<p>Idea: '+idea.body+'</p>' +
                            '' +
                            '</div>' +
                            '</div>';
                    })
                    $('#tagWiseIdea').html(row);
                }
            })
        })
    </script>
@endsection