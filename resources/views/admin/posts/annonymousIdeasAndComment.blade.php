@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">

        <div class="panel-heading">
            <span class="heading">Ideas with no comments </span>
            <button style="margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
            <div class="clearfix"></div>
        </div>

        <div class="col-md-12">

            <div class="col-md-6">
                <div class="panel-body">
                    <h3>Anonymous Idea list</h3>
                    <hr>
                    <span for="" class="label label-primary"> Total Anonymous Idea {{sizeof($annonidea)}}</span>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Posted by</th>
                            <th>Idea</th>
                        </tr>
                        </thead>
                        <tbody>

                        @php
                                $i=1;
                        @endphp
                        @foreach($annonidea as $annidea)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    {{$annidea->user->name}}
                                </td>
                                <td>
                                    {{$annidea->body}}
                                </td>
                                <td class="hidden-print">
                                    <a  href=""  class="btn btn-success" id="ideabutton">View details</a>
                                </td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$annonidea->links()}}
            </div>

            <div class="col-md-6">
                <div class="panel-body">
                    <h3>Anonymous Comment list</h3>
                    <hr>
                    <span for="" class="label label-primary"> Total Anonymous comment {{sizeof($annonComment)}}</span>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Commented by</th>
                            <th>Comment</th>
                            <th>Idea</th>
                        </tr>
                        </thead>
                        <tbody>

                        @php
                            $i=1;
                        @endphp
                        @foreach($annonComment as $anncmnt)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    {{$anncmnt->user->name}}
                                </td>
                                <td>
                                    {{$anncmnt->post->body}}
                                </td>
                                <td>
                                    {{$anncmnt->comment}}
                                </td>
                                <td class="hidden-print">
                                    <a  href=""  class="btn btn-success" id="ideabutton">View details</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$annonComment->links()}}
            </div>
        </div>
    </div>
@endsection