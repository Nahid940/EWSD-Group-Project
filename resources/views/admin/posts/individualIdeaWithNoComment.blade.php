@extends('layouts.admin')
@section('content')

    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"><h3>Details of individual idea</h3></div>
            <div class="panel-body">

                <div class="col-md-12">
                    <div class="col-md-6">
                        <img src="{{asset('uploads/users/'.$d->user->image)}}" alt="">
                    </div>
                </div>

                <table class="table">
                    <tr>
                        <td>Name: {{$d->user->name}}</td>
                    </tr>

                    <tr>
                        <td>Department: {{$info->department->name}}</td>
                    </tr>
                    <tr>
                        <td>Session: {{$info->session->name}}</td>
                    </tr>

                    <tr>
                        <td>Batch: {{$info->batch->name}}</td>
                    </tr>

                    <tr>
                        <td>Idea title: {{$d->title}}</td>
                    </tr>
                    <tr>
                        <td>Idea : {{$d->body}}</td>
                    </tr>
                </table>


            </div>
        </div>
    </div>
@endsection