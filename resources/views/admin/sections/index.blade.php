@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">SECTIONS</span>
        <button id="btnCreate" type="button" class="btn btn-default pull-right hidden-print" data-toggle="modal" data-target="#createModal"><i class="glyphicon glyphicon-plus"> </i> Add New Section</button>
        <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        <button id="btnRefresh" class="btn btn-success pull-right hidden-print" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></button>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="30%">Batch Name</th>
                    <th width="30%">Section Name</th>
                    <th width="10%">Status</th>
                    <th class="hidden-print">Action</th>
                </tr>
            </thead>
            <tbody id="sectionRows">
                
            </tbody>
        </table>
      </div>
    </div>

    <!--Create Modal-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form id="createAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Add New Batch</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
                <label for="name">Batch Name</label>
                <div class="form-group">
                  <select class="form-control batch_id" id="batch" name="batch_id">
                      
                  </select>
                </div>    
                <label for="name">Section Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Session Name">
                </div>         
                <label for="description">Description</label>
                <div class="form-group">
                  <textarea cols="5" rows="4" class="form-control" name="description"></textarea>
                </div>   
                <label for="status">Status</label>
                <div class="form-group">
                  <select class="form-control" name="status" id="status">
                    <option value="0">Active</option>
                    <option value="1">Inactive</option>
                  </select>
                </div>      
                <br>                     
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btndeptsave"> <i class="glyphicon glyphicon-ok" > </i>  Save</button>
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!--Edit Modal-->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form class="editAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Edit Batch</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
               <label for="name">Batch Name</label>
                <div class="form-group">
                  <select class="form-control batch_id" id="batch" name="batch_id">
                      
                  </select>
                </div>    
                <label for="name">Section Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Session Name">
                </div>         
                <label for="description">Description</label>
                <div class="form-group">
                  <textarea id="description" cols="5" rows="4" class="form-control" name="description"></textarea>
                </div>   
                <label for="status">Status</label>
                <div class="form-group">
                  <select class="form-control" name="status" id="status">
                    <option value="0">Active</option>
                    <option value="1">Inactive</option>
                  </select>
                </div>      
                <br>                     
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btnUpdateSection"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>   
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <!--Show Modal-->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Batch Details</h4>
          </div>
          <div class="modal-body">
            <p id="body"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--Message Modal-->
    <div class="modal fade" id="deleteConfirmationModel" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p id="body">Are You Sure to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>            
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
@endsection

@section('script')

    <script>
        $(document).ready(function() {
            $('#alertMessageContainer').css({'display':'none'});
            $('#btnRefresh').on('click', function() {
                loadSection();
            });

            loadSection();

            function loadSection() {
                $.ajax({
                    url: "{{ route('sections.index') }}",
                    type: 'GET',
                    success: function(sections) {
                        var currentSections = sections;
                        //console.log(currentSections);
                        var resultContainer = $('#sectionRows');
                        var rows = '', showUrl = '', token = '', number = 1, status = '';
                        currentSections.map(function(section) {
                            showUrl = ' {{ route('sections.show', ['section' => ':sectionId']) }}';
                            showUrl = showUrl.replace(':sectionId', section.id);
                            //console.log(session.id);
                            if(section.status == 0){
                                status = '<span class="label label-success">Active</span>';
                            }else{
                                status = '<span class="label label-danger">Inactive</span>';
                            }
                            
                            rows += '<tr>' +
                                    '<td>' + number++ + '</td>' +
                                    '<td>' + section.batch.name + '</td>' +
                                    '<td>' + section.name + '</td>' +
                                    '<td>' + status + '</td>' +
                                    '<td>' + 
                                    '<button type="button" class="btnShow hidden-print btn btn-success glyphicon glyphicon-eye-open" id="show" title="Show" data-id="'+ section.id +'"></button>' + ' '+
                                    '<button type="button" class="btnEdit hidden-print btn btn-warning glyphicon glyphicon-edit" title="Edit" id="edit" value="'+ section.id +'"></button>' + ' ' +
                                    '<button type="button" class="btnDelete hidden-print btn btn-danger glyphicon glyphicon-trash" title="Delete" value="'+ section.id +'"></button>' 
                                    + '</td>' +
                                    '</tr>'
                        });
                        resultContainer.html(rows);
                        //$('.btnShow').on('click', openShowModal);
                        $('.btnEdit').on('click', openEditModal);
                        $('.btnDelete').on('click', showDeleteConfirmation);
                    }
                });
            }


            $('#btnCreate').on('click', function() {
              $.ajax({
                  url: "{{ route('sections.create') }}",
                  type: 'GET',
                  success: function (results) {
                    //console.log(results);
                    var currentSections = results;
                    var resultContainer = $('.batch_id');
                    var option = "";
                    results.map(function(result) {
                      //console.log(result.id)
                      option += "<option value='" + result.id +"'>"+ result.name +"</option>"
                      //console.log(option);
                    });
                    resultContainer.html(option);
                  }
              });
            });


            $('#createAjax').on('submit', function(e)  {
                e.preventDefault();

                $.ajax({
                    url: "{{ route('sections.store') }}",
                    type: 'POST',
                    data: $("#createAjax").serialize(),
                    //console.log(replyObject);
                    success: function (result) {
                        var replyObject = JSON.parse(result);
                        //console.log(replyObject.alertType);
                        //$('#alertMessageContainer').css({'display':'block'});

                        $('#alertMessageContainer').addClass(replyObject.alertType);
                        $('#alertMessage').html(replyObject.alertMessage);
                        $('alertMessageContainer').show();
                        $('#createAjax')[0].reset();
                        loadSection();
                    }
                });
            });

           $(document).on('click','#show',function () {
               var id=($(this).data('id'));
               $.ajax({
                   url: '{{ route('sections.show', ['section' => 'id']) }}',
                   type: 'get',
                   data:{
                       _token:'{{csrf_token()}}',
                       id:id
                   },

                   success: function(result) {
                    //console.log(result);
                       var status = '';
                       if(result.status == 0) {
                           status =  '<span class="label label-success">Active</span>';
                       }else{
                           status =  '<span class="label label-danger">Inactive</span>';
                       }
                       $('#body').html([
                           'Batch Name: ' + result.batch.name + '<br>',
                           'Section Name: ' + result.name + '<br>',
                           'Description: ' + result.description + '<br>',
                           'Status: ' + status + ' <br>',
                       ]);
                   }
               });
               $('#showModal').modal();
           });



            function openEditModal() {
                var editableSectionId = $(this).val();
                //console.log(editableSessionDepartmentId);
                var showUrl = '{{ route('sections.edit', ['section' => ':sectionId']) }}';
                showUrl = showUrl.replace(':sectionId', editableSectionId);
                //console.log(sessionDepartmentId);
                $.ajax({
                    url: showUrl,
                    type: 'GET',
                    data:{
                       id:editableSectionId
                   },

                    success: function(replyObject) {
                        var result = JSON.parse(replyObject);
                        //console.log(result[1][1].name);
                        var options = '';
                        var option = "<option value='" + result[0].batch_id +"'>"+ result[0].batch.name +"</option>"
                        result[1].map(function(batchResult) {

                         options += '<option value="'+ batchResult.id +'">'+ batchResult.name +'</option>';                     
                        });
                        $('.editAjax #batch').html(option+options);                        
                        $('.editAjax #name').val(result[0].name);
                        $('.editAjax #description').val(result[0].description);
    
                        $('#btnUpdateSection').val(editableSectionId);
                        $('#editModal').modal();
                    }
                });
            }

            
            function updateSection() {
                var updateableSectionId = $(this).val();
                var updateUrl = '{{ route('sections.update', ['section' => ':sectionId']) }}';
                updateUrl = updateUrl.replace(':sectionId', updateableSectionId);
               // var theTokenVal = '{{ csrf_token() }}';
                var formData = $("form.editAjax").serialize();
                //alert(formData);
                $.ajax({
                    url: updateUrl,
                    type: 'POST',
                    data: formData,
                    success: function(result) {
                      //alert(result);
                        //console.log(result);  
                        loadSection();               
                    }
                });
            }

            $('#btnUpdateSection').on('click', updateSection);

            function showDeleteConfirmation() {
                var deleteSessionDepartmentId = $(this).val();
                $('#deleteButton').val(deleteSessionDepartmentId);
                $('#deleteConfirmationModel').modal();
            }

            $('#deleteButton').on('click', deleteSection);

            function deleteSection() {
                var deleteableSectionId = $(this).val();
                var deleteUrl = '{{ route('sections.destroy', ['section' => ':sectionId']) }}';
                deleteUrl = deleteUrl.replace(':sectionId', deleteableSectionId);

                $.ajax({
                    url : deleteUrl,
                    type : 'POST',
                    data : {
                        "_token": '{{ csrf_token() }}',
                        "_method": 'DELETE'
                    },
                    success: function(result) {
                        loadSection();
                        $('#deleteConfirmationModel').modal('hide');
                    }
                });
            }
        });
    </script>

@endsection