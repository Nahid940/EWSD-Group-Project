@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
        <div class="panel-heading">
            <span class="heading">All the departments</span>
            <button style="margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Validity</th>
                        <th>Department name</th>
                        <th >Session name</th>
                        <th >Start date of session</th>
                        <th >End date of session</th>
                        <th >Closure date of idea post</th>
                        <th >Final close date of idea post</th>
                        <th >Enable/Disable idea post</th>
                    </tr>
                    </thead>
                        <tbody>
                        @foreach($data as $d)
                            <tr>
                                <td>1</td>
                                <td>
                                    @if($d->sessionend_date > \Carbon\Carbon::now())
                                        <span for="" class="label label-success">On going</span>
                                        @elseif($d->end_date < \Carbon\Carbon::now())
                                        <span for="" class="label label-danger">Session over</span>
                                    @endif
                                </td>
                                <td>{{ $d->department->name }}</td>
                                <td>{{ $d->name }}</td>
                                <td>
                                    {{ $d->start_date }}
                                </td>
                                <td>
                                    {{ $d->end_date }}
                                </td>
                                <td>
                                    @if($d->contorlIdeaPost->closure_date >= date('Y-m-d'))
                                        <i class="fas fa-check" style="color: green;font-size: 30px"></i>
                                        Can post
                                    @else
                                        Can't post idea
                                        <i class="fas fa-times" style="color: red;font-size: 30px"></i>
                                    @endif
                                </td>
                                <td>
                                    @if($d->contorlIdeaPost->final_close < date('Y-m-d'))
                                        <i class="fas fa-times" style="color: red;font-size: 30px"></i>
                                        Finally Closed
                                    @else
                                        <i class="fas fa-check" style="color: green;font-size: 30px"></i>
                                        Not closed yet !
                                    @endif
                                </td>
                                <td class="hidden-print">
                                    <a type="button"  class="btn btn-success glyphicon glyphicon-off" data-toggle="modal" data-target="#myModal" title="Action" data-id="{{$d->id}}" id="ideabutton"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                </table>
        </div>
    </div>

    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="sessionid">
                    <dov class="row">
                        <div class="form-group">
                            <label for="">Closure date</label>
                            <input type="date" name="closure_date" id="closure_date" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Final close</label>
                            <input type="date" name="final_close" id="final_close" class="form-control">

                        </div>
                    </dov>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="ideacontrolsubmit">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).on('click','#ideabutton',function () {
            $.ajax({
                type:'post',
                url:'{{route('getsessiondata')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    id:$('#ideabutton').data('id')
                },
                success:function (data) {
                    $('#sessionid').val(data.session_id);
                    $('#closure_date').val(data.closure_date);
                    $('#final_close').val(data.final_close);
                }
            })
        })
        
        $(document).on('click','#ideacontrolsubmit',function () {
            $.ajax({
                type:'post',
                url:'{{route('manageideapost')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    closure_date:$('#closure_date').val(),
                    final_close:$('#final_close').val(),
                    id:$('#sessionid').val()
                },
                success:function (data) {
                    window.location.reload();
                }
            })
        })
    </script>
@endsection