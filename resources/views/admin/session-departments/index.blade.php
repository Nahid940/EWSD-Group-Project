@extends('layouts.admin')

@section('content')

    <div class="panel panel-primary panel-top">
      <div class="panel-heading">
        <span class="heading">SESSION</span>
        <button id="btnCreate" type="button" class="btn btn-default pull-right hidden-print" data-toggle="modal" data-target="#createModal"><i class="glyphicon glyphicon-plus"> </i> Add New Session</button>
        <button style="margin-left:5px;margin-right:5px;" title="Print" type="button" onclick="window.print();" class="btn btn-info pull-right hidden-print"><i class="glyphicon glyphicon-print"></i></button>
        <button id="btnRefresh" class="btn btn-success pull-right hidden-print" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></button>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="20%">Department Name</th>
                    <th width="20%">Session Name</th>
                    <th width="10%">Status</th>
                    <th width="10%">Student can post idea</th>
                    <th width="10%">Final close</th>
                    <th class="hidden-print">Action</th>
                </tr>
            </thead>
            <tbody id="sessionRows">
                
            </tbody>
        </table>
      </div>
    </div>

    <!--Create Modal-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form id="createAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Add New Session</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
               {{--  <input type="hidden" name="_token" value=""> --}}
                <label for="name">Department Name</label>
                <div class="form-group">
                  <select class="form-control department_id" name="department_id">
                      
                  </select>
                </div>    
                <label for="name">Session Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Session Name">
                </div>         
                <label for="start_date">Start Date</label>
                <div class="form-group">
                  <input type="date" name="start_date" id="start_date" class="form-control">
                </div>  
                <label for="end_date">End Date</label>
                <div class="form-group">
                  <input type="date" name="end_date" id="end_date" class="form-control">
                </div>  
                <label for="status">Status</label>
                <div class="form-group">
                  <select class="form-control" name="status">
                    <option value="0">Active</option>
                    <option value="1">Inactive</option>
                  </select>
                </div>      
                <br>                  
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btndeptsave"> <i class="glyphicon glyphicon-ok" > </i>  Save</button>
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!--Edit Modal-->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <form class="editAjax">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Edit Department</h4>
          </div>
          <div class="modal-body">
                <div id="alertMessageContainer" class="alert alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <em id="alertMessage"></em>
                </div>
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
               <label for="name">Department Name</label>
                <div class="form-group">
                  <select class="form-control department_id" id="department" name="department_id">
                      
                  </select>
                </div>    
                <label for="name">Session Name</label>
                <div class="form-group">
                  <input type="text" name="name" id="name" class="form-control" placeholder="Enter Session Name">
                </div>         
                <label for="start_date">Start Date</label>
                <div class="form-group">
                  <input type="date" name="start_date" id="start_date" class="form-control">
                </div>  
                <label for="end_date">End Date</label>
                <div class="form-group">
                  <input type="date" name="end_date" id="end_date" class="form-control">
                </div>

                  <div class="form-group">
                      <label>Student can post idea</label>
                      <select class="form-control" name="can_post" id="can_post">
                          <option value="no">No</option>
                          <option value="yes">Yes</option>
                      </select>
                  </div>
              <div class="form-group">
                  <label for="">Idea submission closed</label>
                  <select class="form-control" name="final_close" id="final_close">
                      <option value="no">No</option>
                      <option value="yes">Yes</option>
                  </select>
              </div>

              <label for="status">Status</label>
                <div class="form-group">
                  <select class="form-control" name="status" id="status">
                    <option value="0">Active</option>
                    <option value="1">Inactive</option>
                  </select>
                </div>      
                <br>                     
          </div>
          <div class="modal-footer">
             <button type="submit" class="btn btn-primary" id="btnUpdateSessionDepartment"> <i class="glyphicon glyphicon-ok"> </i>  Update</button>   
           
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                
          </div>
        </div><!-- /.modal-content -->
        </form>
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



    <!--Show Modal-->
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="heading">Session Details</h4>
          </div>
          <div class="modal-body">
            <p id="body"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!--Message Modal-->
    <div class="modal fade" id="deleteConfirmationModel" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <p id="body">Are You Sure to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" id="deleteButton">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>            
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
@endsection

@section('script')

    <script>
        $(document).ready(function() {
            $('#alertMessageContainer').css({'display':'none'});
            $('#btnRefresh').on('click', function() {
                loadSession();
            });

            loadSession();

            function loadSession() {
                $.ajax({
                    url: "{{ route('session-departments.index') }}",
                    type: 'GET',
                    success: function(sessions) {
                        var currentSessions = sessions;
                        var resultContainer = $('#sessionRows');
                        var rows = '', showUrl = '', token = '', number = 1, status = '';
                        currentSessions.map(function(session) {
                            showUrl = ' {{ route('session-departments.show', ['session' => ':sessionId']) }}';
                            showUrl = showUrl.replace(':sessionId', session.id);
                            //console.log(session.id);
                            if(session.status == 0){
                                status = '<span class="label label-success">Active</span>';
                            }else{
                                status = '<span class="label label-danger">Inactive</span>';
                            }
                            
                            rows += '<tr>' +
                                    '<td>' + number++ + '</td>' +
                                    '<td>' + session.department.name + '</td>' +
                                    '<td>' + session.name + '</td>' +
                                    '<td>' + status + '</td>' +
                                    '<td>' + session.can_post + '</td>' +
                                    '<td>' + session.final_close + '</td>' +
                                    '<td>' +
                                    '<button type="button" class="btnShow hidden-print btn btn-success glyphicon glyphicon-eye-open" id="show" title="Show" data-id="'+ session.id +'"></button>' + ' '+
                                    '<button type="button" class="btnEdit hidden-print btn btn-warning glyphicon glyphicon-edit" title="Edit" id="edit" value="'+ session.id +'"></button>' + ' ' +
                                    '<button type="button" class="btnDelete hidden-print btn btn-danger glyphicon glyphicon-trash" title="Delete" value="'+ session.id +'"></button>' 
                                    + '</td>' +
                                    '</tr>'
                        });
                        resultContainer.html(rows);
                        //$('.btnShow').on('click', openShowModal);
                        $('.btnEdit').on('click', openEditModal);
                        $('.btnDelete').on('click', showDeleteConfirmation);
                    }
                });
            }


            $('#btnCreate').on('click', function() {
              $.ajax({
                  url: "{{ route('session-departments.create') }}",
                  type: 'GET',
                  success: function (results) {
                    //console.log(results);
                    var currentDepartments = results;
                    var resultContainer = $('.department_id');
                    var option = "";
                    results.map(function(result) {
                      option += "<option value='" + result.id +"'>"+ result.name +"</option>"
                    });
                    resultContainer.html(option);
                  }
              });
            });


            $('#createAjax').on('submit', function(e)  {
                e.preventDefault();

                $.ajax({
                    url: "{{ route('session-departments.store') }}",
                    type: 'POST',
                    data: $("#createAjax").serialize(),
                    //console.log(replyObject);
                    success: function (result) {
                        var replyObject = JSON.parse(result);
                        //console.log(replyObject.alertType);
                        //$('#alertMessageContainer').css({'display':'block'});

                        $('#alertMessageContainer').addClass(replyObject.alertType);
                        $('#alertMessage').html(replyObject.alertMessage);
                        $('alertMessageContainer').show();
                        $('#createAjax')[0].reset();
                        loadSession();
                    },
                    error:function (err) {
                        alert(err.responseText);
                    }
                });
            });

           $(document).on('click','#show',function () {
               var id=($(this).data('id'));
               $.ajax({
                   url: '{{ route('session-departments.show', ['session' => 'id']) }}',
                   type: 'get',
                   data:{
                       _token:'{{csrf_token()}}',
                       id:id
                   },

                   success: function(result) {
                    //console.log(result);
                       var status = '';
                       if(result.status == 0) {
                           status =  '<span class="label label-success">Active</span>';
                       }else{
                           status =  '<span class="label label-danger">Inactive</span>';
                       }
                       $('#body').html([
                           'Department Name: ' + result.department.name + '<br>',
                           'Session Name: ' + result.name + '<br>',
                           'Start Date: ' + result.start_date + '<br>',
                           'End Date: ' + result.end_date + '<br>',
                           'Status: ' + status + ' <br>',
                       ]);
                   }
               });
               $('#showModal').modal();
           });



            function openEditModal() {
                var editableSessionDepartmentId = $(this).val();
                //console.log(editableSessionDepartmentId);
                var showUrl = '{{ route('session-departments.edit', ['sessionDepartment' => ':sessionDepartmentId']) }}';
                showUrl = showUrl.replace(':sessionDepartmentId', editableSessionDepartmentId);
                //console.log(sessionDepartmentId);
                $.ajax({
                    url: showUrl,
                    type: 'GET',
                    data:{
                       id:editableSessionDepartmentId
                   },

                    success: function(replyObject) {
                        var result = JSON.parse(replyObject);
                        //console.log(result[1][1].name);
                        var options = '';
                        var option = "<option value='" + result[0].department_id +"'>"+ result[0].department.name +"</option>"
                        result[1].map(function(departmentResult) {

                         options += '<option value="'+ departmentResult.id +'">'+ departmentResult.name +'</option>';                     
                        });
                        $('.editAjax #department').html(option+options);                        
                        $('.editAjax #name').val(result[0].name);
                        $('.editAjax #start_date').val(result[0].start_date);
                        $('.editAjax #end_date').val(result[0].end_date);
                        $('.editAjax #final_close').val(result[0].final_close);
                        $('.editAjax #can_post').val(result[0].can_post);

                        $('#btnUpdateSessionDepartment').val(editableSessionDepartmentId);
                        $('#editModal').modal();
                    }
                });
            }

            
            function updateSessionDepartment() {
                var updateableSessionDepartmentId = $(this).val();
                var updateUrl = '{{ route('session-departments.update', ['sessionDepartment' => ':sessionDepartmentId']) }}';
                updateUrl = updateUrl.replace(':sessionDepartmentId', updateableSessionDepartmentId);
               // var theTokenVal = '{{ csrf_token() }}';
                var formData = $("form.editAjax").serialize();
                //alert(formData);
                $.ajax({
                    url: updateUrl,
                    type: 'POST',
                    data: formData,
                    success: function(result) {
                        //console.log(result);  
                        loadSession();               
                    }
                });
            }

            $('#btnUpdateSessionDepartment').on('click', updateSessionDepartment);

            function showDeleteConfirmation() {
                var deleteSessionDepartmentId = $(this).val();
                $('#deleteButton').val(deleteSessionDepartmentId);
                $('#deleteConfirmationModel').modal();
            }

            $('#deleteButton').on('click', deleteSessionDepartment);

            function deleteSessionDepartment() {
                var deleteableSessionDepartmentId = $(this).val();
                var deleteUrl = '{{ route('session-departments.destroy', ['sessionDepartment' => ':sessionDepartmentId']) }}';
                deleteUrl = deleteUrl.replace(':sessionDepartmentId', deleteableSessionDepartmentId);

                $.ajax({
                    url : deleteUrl,
                    type : 'POST',
                    data : {
                        "_token": '{{ csrf_token() }}',
                        "_method": 'DELETE'
                    },
                    success: function(result) {
                        loadSession();
                        $('#deleteConfirmationModel').modal('hide');
                    }
                });
            }
        });
    </script>

@endsection