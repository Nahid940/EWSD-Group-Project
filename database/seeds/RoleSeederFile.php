<?php

use Illuminate\Database\Seeder;

class RoleSeederFile extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role=new \App\Role();
        $role->designation='Admin';
        $role->role_name='admin';
        $role->salary=25000;
        $role->save();

        $role1=new \App\Role();
        $role1->designation='Student';
        $role1->role_name='student';
        $role1->salary=0;
        $role1->save();

        $role2=new \App\Role();
        $role2->designation='Quality Assurance Manager';
        $role2->role_name='qualityassurancemanager';
        $role2->salary=250000;
        $role2->save();

        $role3=new \App\Role();
        $role3->designation='QA coordinator';
        $role3->role_name='qacoordinator';
        $role3->salary=250000;
        $role3->save();
    }
}
