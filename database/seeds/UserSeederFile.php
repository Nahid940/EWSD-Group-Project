<?php

use Illuminate\Database\Seeder;

class UserSeederFile extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user=new \App\User();
        $user->role_id=1;
        $user->name='Admin';
        $user->email='admin@gmail.com';
        $user->address='Dhaka';
        $user->phone='01874680376';
        $user->image='1519725897.jpg';
        $user->status=1;
        $user->password=bcrypt(123456);
        $user->save();


        $user3=new \App\User();
        $user3->role_id=3;
        $user3->name='QA Manager';
        $user3->email='qam@gmail.com';
        $user3->address='Dhaka';
        $user3->phone='43565463';
        $user3->image='1519725897.jpg';
        $user3->status=1;
        $user3->password=bcrypt(123456);
        $user3->save();

    }
}
